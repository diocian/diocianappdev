package com.diocian.diocianappdev.model;

import com.diocian.diocianappdev.util.GlobalConstants;

import org.json.JSONException;
import org.json.JSONObject;

public class Playlist extends Base {

    private String playlist_sn;
    private String playlist_group_sn;
    private String video_yn;
    private String target_dc;
    private String target_sn;
    private String order_sn;
    private String prev_playlist_sn = "0";
    private String creator_sn;
    private String title;
    private String title_i18n_key;
    private String artist_name;
    private String artist_name_i18n_key;
    private String price;
    private String US_price;
    private String JP_price;
    private String KR_price;
    private String VN_price;
    private String youtube_video_id;
    private String member_name;
    private String member_image_url;
    private String audio_file_url;
    private String image_file_url;
    private String my_like_count;
    private String cover_image_url;

    public Playlist setPlaylistFromJSONObject(JSONObject playlist) throws JSONException {
        this.playlist_sn = playlist.has("playlist_sn") ? playlist.getString("playlist_sn") : "";
        this.playlist_group_sn = playlist.has("playlist_group_sn") ? playlist.getString("playlist_group_sn") : "";
        this.video_yn = playlist.has("video_yn") ? playlist.getString("video_yn") : "";
        this.target_sn = playlist.has("target_sn") ? playlist.getString("target_sn") : "";
        this.order_sn = playlist.has("order_sn") ? playlist.getString("order_sn") : "";
        this.creator_sn = playlist.has("creator_sn") ? playlist.getString("creator_sn") : "";
        this.target_dc = playlist.has("target_dc") ? playlist.getString("target_dc") : "";
        this.creator_sn = playlist.has("creator_sn") ? playlist.getString("creator_sn") : "";
        this.title = playlist.has("title") ? playlist.getString("title") : "";
        this.title_i18n_key = playlist.has("title_i18n_key") ? playlist.getString("title_i18n_key") : "";
        this.artist_name = playlist.has("artist_name") ? playlist.getString("artist_name") : "";
        this.artist_name_i18n_key = playlist.has("artist_name_i18n_key") ? playlist.getString("artist_name_i18n_key") : "";
        this.price = playlist.has("price") ? playlist.getString("price") : "";
        this.US_price = playlist.has("US_price") ? playlist.getString("US_price") : "";
        this.JP_price = playlist.has("JP_price") ? playlist.getString("JP_price") : "";
        this.KR_price = playlist.has("KR_price") ? playlist.getString("KR_price") : "";
        this.VN_price = playlist.has("VN_price") ? playlist.getString("VN_price") : "";
        this.youtube_video_id = playlist.has("youtube_video_id") ? playlist.getString("youtube_video_id") : "";
        this.member_name = playlist.has("member_name") ? playlist.getString("member_name") : "";
        this.member_image_url = playlist.has("member_image_url") ? playlist.getString("member_image_url") : "";
        this.audio_file_url = playlist.has("audio_file_url") ? GlobalConstants.DOMAIN + playlist.getString("audio_file_url") : "";
        this.image_file_url = playlist.has("image_file_url") ? playlist.getString("image_file_url") : "";
        this.my_like_count = playlist.has("my_like_count") ? playlist.getString("my_like_count") : "";
        this.cover_image_url = playlist.has("cover_image_url") ? GlobalConstants.DOMAIN + playlist.getString("cover_image_url") : "";
        return this;
    }

    public String getPlaylist_sn() {
        return playlist_sn;
    }

    public void setPlaylist_sn(String playlist_sn) {
        this.playlist_sn = playlist_sn;
    }

    public String getPlaylist_group_sn() {
        return playlist_group_sn;
    }

    public void setPlaylist_group_sn(String playlist_group_sn) {
        this.playlist_group_sn = playlist_group_sn;
    }

    public String getVideo_yn() {
        return video_yn;
    }

    public void setVideo_yn(String video_yn) {
        this.video_yn = video_yn;
    }

    public String getTarget_dc() {
        return target_dc;
    }

    public void setTarget_dc(String target_dc) {
        this.target_dc = target_dc;
    }

    public String getTarget_sn() {
        return target_sn;
    }

    public void setTarget_sn(String target_sn) {
        this.target_sn = target_sn;
    }

    public String getOrder_sn() {
        return order_sn;
    }

    public void setOrder_sn(String order_sn) {
        this.order_sn = order_sn;
    }

    public String getPrev_playlist_sn() {
        return prev_playlist_sn;
    }

    public void setPrev_playlist_sn(String prev_playlist_sn) {
        this.prev_playlist_sn = prev_playlist_sn;
    }

    @Override
    public String getCreator_sn() {
        return creator_sn;
    }

    @Override
    public void setCreator_sn(String creator_sn) {
        this.creator_sn = creator_sn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle_i18n_key() {
        return title_i18n_key;
    }

    public void setTitle_i18n_key(String title_i18n_key) {
        this.title_i18n_key = title_i18n_key;
    }

    public String getArtist_name() {
        return artist_name;
    }

    public void setArtist_name(String artist_name) {
        this.artist_name = artist_name;
    }

    public String getArtist_name_i18n_key() {
        return artist_name_i18n_key;
    }

    public void setArtist_name_i18n_key(String artist_name_i18n_key) {
        this.artist_name_i18n_key = artist_name_i18n_key;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getUS_price() {
        return US_price;
    }

    public void setUS_price(String US_price) {
        this.US_price = US_price;
    }

    public String getJP_price() {
        return JP_price;
    }

    public void setJP_price(String JP_price) {
        this.JP_price = JP_price;
    }

    public String getKR_price() {
        return KR_price;
    }

    public void setKR_price(String KR_price) {
        this.KR_price = KR_price;
    }

    public String getVN_price() {
        return VN_price;
    }

    public void setVN_price(String VN_price) {
        this.VN_price = VN_price;
    }

    public String getYoutube_video_id() {
        return youtube_video_id;
    }

    public void setYoutube_video_id(String youtube_video_id) {
        this.youtube_video_id = youtube_video_id;
    }

    public String getMember_name() {
        return member_name;
    }

    public void setMember_name(String member_name) {
        this.member_name = member_name;
    }

    public String getMember_image_url() {
        return member_image_url;
    }

    public void setMember_image_url(String member_image_url) {
        this.member_image_url = member_image_url;
    }

    public String getAudio_file_url() {
        return audio_file_url;
    }

    public void setAudio_file_url(String audio_file_url) {
        this.audio_file_url = audio_file_url;
    }

    public String getImage_file_url() {
        return image_file_url;
    }

    public void setImage_file_url(String image_file_url) {
        this.image_file_url = image_file_url;
    }

    public String getMy_like_count() {
        return my_like_count;
    }

    public void setMy_like_count(String my_like_count) {
        this.my_like_count = my_like_count;
    }

    public String getCover_image_url() {
        return cover_image_url;
    }

    public void setCover_image_url(String cover_image_url) {
        this.cover_image_url = cover_image_url;
    }
}
