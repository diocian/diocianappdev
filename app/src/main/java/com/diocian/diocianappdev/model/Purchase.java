package com.diocian.diocianappdev.model;

import android.os.Bundle;
import android.support.v4.app.Fragment;

public class Purchase extends Base {

    private String target_dc;
    private String post_title;
    private String post_sn;
    private String seller_name;
    private String seller_sn;
    private String price;
    private String purchased_date;

    public Purchase setPurchase(String target_dc, String post_title, String post_sn, String seller_name, String seller_sn, String price, String purchased_date) {
        this.target_dc = target_dc;
        this.post_title = post_title;
        this.post_sn = post_sn;
        this.seller_name = seller_name;
        this.seller_sn = seller_sn;
        this.price = price;
        this.purchased_date = purchased_date;
        return this;
    }

    public String getTarget_dc() {
        return target_dc;
    }

    public void setTarget_dc(String target_dc) {
        this.target_dc = target_dc;
    }

    public String getPost_title() {
        return post_title;
    }

    public void setPost_title(String post_title) {
        this.post_title = post_title;
    }

    public String getPost_sn() {
        return post_sn;
    }

    public void setPost_sn(String post_sn) {
        this.post_sn = post_sn;
    }

    public String getSeller_name() {
        return seller_name;
    }

    public void setSeller_name(String seller_name) {
        this.seller_name = seller_name;
    }

    public String getSeller_sn() {
        return seller_sn;
    }

    public void setSeller_sn(String seller_sn) {
        this.seller_sn = seller_sn;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPurchased_date() {
        return purchased_date;
    }

    public void setPurchased_date(String purchased_date) {
        this.purchased_date = purchased_date;
    }
}
