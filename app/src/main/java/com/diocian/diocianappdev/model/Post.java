package com.diocian.diocianappdev.model;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.diocian.diocianappdev.util.GlobalConstants;

import org.json.JSONException;
import org.json.JSONObject;

public class Post extends Base {
    private static final long serialVersionUID = 7690952713704649478L;
    private String title;
    private String genre_name;
    private String job_name;
    private String price;
    private String duration;
    private String artist_name;
    private String image_file_url;
    private String wave_image_file_url;
    private String creator_name;
    private String like_count;
    private String comment_count;
    private String created_datetime;
    private String creator_image_file_url;
    private String audio_file_url;
    private String audio_download_file_url;
    private String post_sn;
    private String youtube_video_id;
    private String my_like_count;
    private String target_dc;
    private String description;
    private String creator_sn;
    private String follow_sn;
    private Fragment redirectFragment;
    private Bundle paramBundle;
    private String US_price;
    private String JP_price;
    private String KR_price;
    private String VN_price;

    public Post setPostFromJSONObject(JSONObject post) throws JSONException {
        this.post_sn = post.has("post_sn") ? post.getString("post_sn") : "";
        this.target_dc = post.has("target_dc") ? post.getString("target_dc") : "";
        this.title = post.has("title") ? post.getString("title") : "";
        this.artist_name = post.has("artist_name") ? post.getString("artist_name") : "";
        this.description = post.has("description") ? post.getString("description") : "";
        this.genre_name = post.has("genre_name") ? post.getString("genre_name") : "";
        this.job_name = post.has("job_name") ? post.getString("job_name") : "";
        this.image_file_url = post.has("cover_image_url") ? GlobalConstants.DOMAIN + post.getString("cover_image_url") : "";
        this.wave_image_file_url = post.has("image_file_url") ? GlobalConstants.DOMAIN + post.getString("image_file_url") : "";
        this.price = post.has("price") ? post.getString("price") : "";
        this.US_price = post.has("US_price") ? post.getString("US_price") : "";
        this.JP_price = post.has("JP_price") ? post.getString("JP_price") : "";
        this.KR_price = post.has("KR_price") ? post.getString("KR_price") : "";
        this.VN_price = post.has("VN_price") ? post.getString("VN_price") : "";
        this.audio_file_url = post.has("audio_file_url") ? GlobalConstants.DOMAIN + post.getString("audio_file_url") : "";
        this.audio_download_file_url = post.has("audio_file_url") ? post.getString("audio_file_url") : "";
        this.duration = post.has("play_sec") ? post.getString("play_sec") : "";
        this.youtube_video_id = post.has("youtube_video_id") ? post.getString("youtube_video_id") : "";
        this.my_like_count = post.has("my_like_count") ? post.getString("my_like_count") : "";
        this.like_count = post.has("like_sn") ? post.getString("like_sn") : "";
        this.comment_count = post.has("comment_sn") ? post.getString("comment_sn") : "";
        this.follow_sn = post.has("follow_sn") ? post.getString("follow_sn") : "";
        this.creator_sn = post.has("creator_sn") ? post.getString("creator_sn") : "";
        this.creator_name = post.has("member_name") ? post.getString("member_name") : "";
        this.creator_image_file_url = post.has("member_image_url") ? GlobalConstants.DOMAIN + post.getString("member_image_url") : "";
        this.created_datetime = post.has("created_datetime") ? post.getString("created_datetime") : "";
        return this;
    }

    public Post setArticle(String post_sn, String image_file_url, String title, String creator_name, String like_count, String comment_count, String created_datetime, String creator_image_file_url, String description, String creator_sn, String my_like_count, String follow_sn) {
        this.target_dc = "article";
        this.post_sn = post_sn;
        this.title = title;
        this.creator_name = creator_name;
        this.image_file_url = image_file_url;
        this.like_count = like_count;
        this.comment_count = comment_count;
        this.created_datetime = created_datetime;
        this.creator_image_file_url = creator_image_file_url;
        this.description = description;
        this.creator_sn = creator_sn;
        this.my_like_count = my_like_count;
        this.follow_sn = follow_sn;
        return this;
    }

    public Post setStudynote(String post_sn, String image_file_url, String genre_name, String job_name, String price, String duration, String wave_image_file_url, String title, String creator_name, String audio_file_url) {
        this.target_dc = "study note";
        this.post_sn = post_sn;
        this.image_file_url = image_file_url;
        this.genre_name = genre_name;
        this.job_name = job_name;
        this.price = price;
        this.duration = duration;
        this.wave_image_file_url = wave_image_file_url;
        this.title = title;
        this.creator_name = creator_name;
        this.audio_file_url = audio_file_url;
        return this;
    }

    public Post setStudynote(String post_sn, String image_file_url, String genre_name, String job_name, String price, String duration, String wave_image_file_url, String title, String creator_name, String like_count, String comment_count, String created_datetime, String creator_image_file_url, String audio_file_url, String creator_sn, String my_like_count, String follow_sn) {
        this.target_dc = "study note";
        this.post_sn = post_sn;
        this.image_file_url = image_file_url;
        this.genre_name = genre_name;
        this.job_name = job_name;
        this.price = price;
        this.duration = duration;
        this.wave_image_file_url = wave_image_file_url;
        this.title = title;
        this.creator_name = creator_name;
        this.like_count = like_count;
        this.comment_count = comment_count;
        this.created_datetime = created_datetime;
        this.creator_image_file_url = creator_image_file_url;
        this.audio_file_url = audio_file_url;
        this.creator_sn = creator_sn;
        this.my_like_count = my_like_count;
        this.follow_sn = follow_sn;
        return this;
    }

    public Post setStudio(String post_sn, String genre_name, String job_name, String title, String creator_name) {
        this.target_dc = "studio";
        this.post_sn = post_sn;
        this.genre_name = genre_name;
        this.job_name = job_name;
        this.title = title;
        this.creator_name = creator_name;
        return this;
    }

    public Post setStudio(String post_sn, String genre_name, String job_name, String title, String creator_name, String image_file_url, String like_count, String comment_count, String created_datetime, String creator_image_file_url, String creator_sn, String my_like_count, String follow_sn) {
        this.target_dc = "studio";
        this.post_sn = post_sn;
        this.genre_name = genre_name;
        this.job_name = job_name;
        this.title = title;
        this.creator_name = creator_name;
        this.like_count = like_count;
        this.comment_count = comment_count;
        this.created_datetime = created_datetime;
        this.creator_image_file_url = creator_image_file_url;
        this.image_file_url = image_file_url;
        this.creator_sn = creator_sn;
        this.my_like_count = my_like_count;
        this.follow_sn = follow_sn;
        return this;
    }

    public Post setMusic(String post_sn, String genre_name, String image_file_url, String title, String artist_name, String audio_file_url) {
        this.target_dc = "music";
        this.post_sn = post_sn;
        this.title = title;
        this.artist_name = artist_name;
        this.image_file_url = image_file_url;
        this.genre_name = genre_name;
        this.audio_file_url = audio_file_url;
        return this;
    }

    public Post setMusic(String post_sn, String title, String artist_name, String image_file_url, String genre_name, String like_count, String comment_count, String creator_name, String creator_image_file_url, String audio_file_url, String creator_sn, String my_like_count, String follow_sn) {
        this.target_dc = "music";
        this.post_sn = post_sn;
        this.title = title;
        this.artist_name = artist_name;
        this.image_file_url = image_file_url;
        this.genre_name = genre_name;
        this.like_count = like_count;
        this.comment_count = comment_count;
        this.creator_name = creator_name;
        this.creator_image_file_url = creator_image_file_url;
        this.audio_file_url = audio_file_url;
        this.creator_sn = creator_sn;
        this.my_like_count = my_like_count;
        this.follow_sn = follow_sn;
        return this;
    }

    public Post setVideo(String youtube_video_id, String post_sn, String genre_name, String image_file_url, String title, String creator_name) {
        this.target_dc = "video";
        this.post_sn = post_sn;
        this.title = title;
        this.creator_name = creator_name;
        this.image_file_url = image_file_url;
        this.genre_name = genre_name;
        this.youtube_video_id = youtube_video_id;
        return this;
    }

//    public Post setVideo(String post_sn, String genre_name, String image_file_url, String title, String creator_name, String like_count, String comment_count, String created_datetime, String creator_image_file_url) {
//        this.post_sn = post_sn;
//        this.title = title;
//        this.creator_name = creator_name;
//        this.image_file_url = image_file_url;
//        this.genre_name = genre_name;
//        this.like_count = like_count;
//        this.comment_count = comment_count;
//        this.created_datetime = created_datetime;
//        this.creator_image_file_url = creator_image_file_url;
//        return this;
//    }

//    public Post setVideo(String youtube_video_id, String post_sn, String genre_name, String image_file_url, String title, String creator_name, String like_count, String comment_count, String created_datetime, String creator_image_file_url, String creator_sn, String follow_sn) {
//        this.target_dc = "video";
//        this.post_sn = post_sn;
//        this.title = title;
//        this.creator_name = creator_name;
//        this.image_file_url = image_file_url;
//        this.genre_name = genre_name;
//        this.like_count = like_count;
//        this.comment_count = comment_count;
//        this.created_datetime = created_datetime;
//        this.creator_image_file_url = creator_image_file_url;
//        this.youtube_video_id = youtube_video_id;
//        this.follow_sn = follow_sn;
//        return this;
//    }

    public Post setVideo(String youtube_video_id, String post_sn, String genre_name, String image_file_url, String title, String creator_name, String like_count, String comment_count, String created_datetime, String creator_image_file_url, String my_like_count, String creator_sn, String follow_sn, String audio_file_url) {
        this.target_dc = "video";
        this.post_sn = post_sn;
        this.title = title;
        this.creator_name = creator_name;
        this.image_file_url = image_file_url;
        this.genre_name = genre_name;
        this.like_count = like_count;
        this.comment_count = comment_count;
        this.created_datetime = created_datetime;
        this.creator_image_file_url = creator_image_file_url;
        this.youtube_video_id = youtube_video_id;
        this.my_like_count = my_like_count;
        this.creator_sn = creator_sn;
        this.follow_sn = follow_sn;
        this.audio_file_url = audio_file_url;
        return this;
    }

    public Post setMarket(String post_sn, String price, String image_file_url, String title, String creator_name) {
        this.target_dc = "market";
        this.post_sn = post_sn;
        this.price = price;
        this.image_file_url = image_file_url;
        this.title = title;
        this.creator_name = creator_name;
        return this;
    }

    public Post setMarket(String post_sn, String price, String image_file_url, String title, String creator_name, String like_count, String comment_count, String created_datetime, String creator_image_file_url, String creator_sn, String my_like_count, String follow_sn) {
        this.target_dc = "market";
        this.post_sn = post_sn;
        this.title = title;
        this.creator_name = creator_name;
        this.image_file_url = image_file_url;
        this.price = price;
        this.like_count = like_count;
        this.comment_count = comment_count;
        this.created_datetime = created_datetime;
        this.creator_image_file_url = creator_image_file_url;
        this.creator_sn = creator_sn;
        this.my_like_count = my_like_count;
        this.follow_sn = follow_sn;
        return this;
    }

    public Post setMr(String post_sn, String title, String artist_name, String image_file_url, String audio_file_url, String youtube_video_id) {
        this.target_dc = "source";
        this.post_sn = post_sn;
        this.title = title;
        this.artist_name = artist_name;
        this.image_file_url = image_file_url;
        this.audio_file_url = audio_file_url;
        this.youtube_video_id = youtube_video_id;
        return this;
    }

    public Post setSource(String post_sn, String title, String artist_name, String image_file_url, String audio_file_url, String youtube_video_id, String audio_download_file_url) {
        this.target_dc = "source";
        this.post_sn = post_sn;
        this.title = title;
        this.artist_name = artist_name;
        this.image_file_url = image_file_url;
        this.audio_file_url = audio_file_url;
        this.youtube_video_id = youtube_video_id;
        this.audio_download_file_url = audio_download_file_url;
        return this;
    }

    public String getArtist_name() {
        return artist_name;
    }

    public void setArtist_name(String artist_name) {
        this.artist_name = artist_name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage_file_url() {
        return image_file_url;
    }

    public void setImage_file_url(String image_file_url) {
        this.image_file_url = image_file_url;
    }

    public String getGenre_name() {
        return genre_name;
    }

    public void setGenre_name(String genre_name) {
        this.genre_name = genre_name;
    }

    public String getJob_name() {
        return job_name;
    }

    public void setJob_name(String job_name) {
        this.job_name = job_name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getWave_image_file_url() {
        return wave_image_file_url;
    }

    public void setWave_image_file_url(String wave_image_file_url) {
        this.wave_image_file_url = wave_image_file_url;
    }

    public String getCreator_name() {
        return creator_name;
    }

    public void setCreator_name(String creator_name) {
        this.creator_name = creator_name;
    }

    public String getLike_count() {
        return like_count;
    }

    public void setLike_count(String like_count) {
        this.like_count = like_count;
    }

    public String getComment_count() {
        return comment_count;
    }

    public void setComment_count(String comment_count) {
        this.comment_count = comment_count;
    }

    @Override
    public String getCreated_datetime() {
        return created_datetime;
    }

    @Override
    public void setCreated_datetime(String created_datetime) {
        this.created_datetime = created_datetime;
    }

    public String getCreator_image_file_url() {
        return creator_image_file_url;
    }

    public void setCreator_image_file_url(String creator_image_file_url) {
        this.creator_image_file_url = creator_image_file_url;
    }

    public String getAudio_file_url() {
        return audio_file_url;
    }

    public void setAudio_file_url(String audio_file_url) {
        this.audio_file_url = audio_file_url;
    }

    public String getPost_sn() {
        return post_sn;
    }

    public void setPost_sn(String post_sn) {
        this.post_sn = post_sn;
    }

    public String getYoutube_video_id() {
        return youtube_video_id;
    }

    public void setYoutube_video_id(String youtube_video_id) {
        this.youtube_video_id = youtube_video_id;
    }

    public String getMy_like_count() {
        return my_like_count;
    }

    public void setMy_like_count(String my_like_count) {
        this.my_like_count = my_like_count;
    }

    public String getTarget_dc() {
        return target_dc;
    }

    public void setTarget_dc(String target_dc) {
        this.target_dc = target_dc;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreator_sn() {
        return creator_sn;
    }

    public void setCreator_sn(String creator_sn) {
        this.creator_sn = creator_sn;
    }

    public String getFollow_sn() {
        return follow_sn;
    }

    public void setFollow_sn(String follow_sn) {
        this.follow_sn = follow_sn;
    }

    public Fragment getRedirectFragment() {
        return redirectFragment;
    }

    public void setRedirectFragment(Fragment redirectFragment) {
        this.redirectFragment = redirectFragment;
    }

    public Bundle getParamBundle() {
        return paramBundle;
    }

    public void setParamBundle(Bundle paramBundle) {
        this.paramBundle = paramBundle;
    }

    public String getAudio_download_file_url() {
        return audio_download_file_url;
    }

    public void setAudio_download_file_url(String audio_download_file_url) {
        this.audio_download_file_url = audio_download_file_url;
    }

    public String getUS_price() {
        return US_price;
    }

    public void setUS_price(String US_price) {
        this.US_price = US_price;
    }

    public String getJP_price() {
        return JP_price;
    }

    public void setJP_price(String JP_price) {
        this.JP_price = JP_price;
    }

    public String getKR_price() {
        return KR_price;
    }

    public void setKR_price(String KR_price) {
        this.KR_price = KR_price;
    }

    public String getVN_price() {
        return VN_price;
    }

    public void setVN_price(String VN_price) {
        this.VN_price = VN_price;
    }
}
