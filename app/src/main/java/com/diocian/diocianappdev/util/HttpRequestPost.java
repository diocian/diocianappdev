package com.diocian.diocianappdev.util;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.diocian.diocianappdev.MainActivity;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.UUID;

public class HttpRequestPost {
    private Context context;

    public HttpRequestPost(Context context) {
        this.context = context;
    }

    public JSONObject httpGetRequest(String url) {
        JSONObject jsonObject;
        try {
            String query = "";
            if (url.indexOf("?") > 0) {
                query = url.substring(url.indexOf("?") + 1, url.length());
                url = url.substring(0, url.indexOf("?"));
            }
            Log.d("TEST----->", "url : " + url);
            Log.d("TEST----->", "query : " + query.replaceAll(System.getProperty("line.separator"),""));
            Log.d("TEST----->", "((MainActivity) context).getPreferences(\"session_id\") : " + ((MainActivity) context).getPreferences("session_id_raw"));

            URL target = new URL(url);
            HttpURLConnection httpURLConnection = (HttpURLConnection) target.openConnection();
            httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            httpURLConnection.setRequestProperty("Cookie", ((MainActivity) context).getPreferences("session_id_raw"));
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setDefaultUseCaches(false);

            OutputStream outputStream = httpURLConnection.getOutputStream();
            outputStream.write(query.getBytes());
            outputStream.flush();
            outputStream.close();

            String headerSession = httpURLConnection.getHeaderField("Set-Cookie");
            InputStream inputStream = new BufferedInputStream(httpURLConnection.getInputStream());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            StringBuilder stringBuilder = new StringBuilder();
            String input;
            while ((input = bufferedReader.readLine()) != null)
                stringBuilder.append(input);
            jsonObject = new JSONObject(stringBuilder.toString());
            if (headerSession == null) headerSession = "";
            jsonObject.put("session_id", headerSession);
            bufferedReader.close();
            inputStream.close();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return jsonObject;
    }

    public HttpGetRequestTask newHttpGetRequestTask(CallbackIf successCallback) {
        return new HttpGetRequestTask(successCallback);
    }

    public class HttpGetRequestTask extends AsyncTask<String, Void, JSONObject> {

        private CallbackIf successCallback;

        public HttpGetRequestTask(CallbackIf successCallback) {
            this.successCallback = successCallback;
        }

        @Override
        protected JSONObject doInBackground(String... url) {
            return httpGetRequest(url[0]);
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            successCallback.callback(jsonObject, context);
        }
    }

    public interface CallbackIf extends Serializable {
        void callback(JSONObject jsonObject, Context context);
    }


}
