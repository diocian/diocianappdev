package com.diocian.diocianappdev.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.URL;

public class SetImageByUrl extends AsyncTask<String, Void, Bitmap> {

    private final WeakReference<ImageView> imageWeakReference;
    int reqWidth = 24;

    public SetImageByUrl(ImageView imageView, int reqWidth) {
        imageWeakReference = new WeakReference<>(imageView);
        int density = (int) imageView.getContext().getResources().getDisplayMetrics().density;
        this.reqWidth = reqWidth * density;
    }

    @Override
    protected Bitmap doInBackground(String... urls) {
        URL url;
        Bitmap bitmap = null;
        try {
            url = new URL(urls[0]);
            bitmap = decodeSampledBitmapFromUrl(url, reqWidth);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        ImageView imageViewUserHome = imageWeakReference.get();
        if (imageViewUserHome != null) {
            imageViewUserHome.setImageBitmap(bitmap);
        }
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth) {
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (width > reqWidth) {
            final int halfWidth = width / 2;
            while ((halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromUrl(URL url, int reqWidth) throws IOException {
        final BitmapFactory.Options options = new BitmapFactory.Options();

        options.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(url.openStream(), null, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth);

        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeStream(url.openStream(), null, options);
    }

}


