package com.diocian.diocianappdev.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.os.Handler;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.MenuItemCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.diocian.diocianappdev.MainActivity;
import com.diocian.diocianappdev.R;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import me.leolin.shortcutbadger.ShortcutBadger;

public class Utils {
    private Utils() {
    }

    public static boolean hasFroyo() {
        return Build.VERSION.SDK_INT >= VERSION_CODES.FROYO;
    }

    public static boolean hasGingerbread() {
        return Build.VERSION.SDK_INT >= VERSION_CODES.GINGERBREAD;
    }

    public static boolean hasHoneycomb() {
        return Build.VERSION.SDK_INT >= VERSION_CODES.HONEYCOMB;
    }

    public static boolean hasHoneycombMR1() {
        return Build.VERSION.SDK_INT >= VERSION_CODES.HONEYCOMB_MR1;
    }

    public static boolean hasJellyBean() {
        return Build.VERSION.SDK_INT >= VERSION_CODES.JELLY_BEAN;
    }

    public static boolean hasKitKat() {
        return Build.VERSION.SDK_INT >= VERSION_CODES.KITKAT;
    }

    public static String getTimeLag(String created_datetime, Context context) throws ParseException {
        @SuppressLint("SimpleDateFormat") final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
        Date created_date = simpleDateFormat.parse(created_datetime);

        Calendar calendar = Calendar.getInstance();
        Date current_date = calendar.getTime();

        float fTimeLag = (current_date.getTime() - created_date.getTime()) / 1000 / 60;
        String timeLag = "";
        if (context != null) {
            if (fTimeLag < 1) {
                timeLag = context.getString(R.string.justbefore);
            } else if ((fTimeLag / 60) < 1) {
                timeLag = Math.round(fTimeLag) + " " + context.getString(R.string.minutesago);
            } else if ((fTimeLag / 60 / 24) < 1) {
                timeLag = Math.round(fTimeLag / 60) + " " + context.getString(R.string.hoursago);
            } else if ((fTimeLag / 60 / 24 / 30) < 1) {
                timeLag = Math.round(fTimeLag / 60 / 24) + " " + context.getString(R.string.daysago);
            } else {
                timeLag = Math.round(fTimeLag / 60 / 24 / 30) + " " + context.getString(R.string.monthsago);
            }
        }

        return timeLag;
    }

    public static int calColumnCount(Context context) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        int width = dm.widthPixels;
        float density = dm.density;
        float widthDp = width / density;
        return (int) ((widthDp - 8) / 176);
    }

    public static void addTextChangedListenerToCheckLengthInTextInputLayout(final TextInputLayout textInputLayout, final int minimumLength, final String errorMessage) {
        textInputLayout.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                checkLengthInTextInputLayout2(textInputLayout, minimumLength, errorMessage);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    public static void checkLengthInTextInputLayout(final TextInputLayout textInputLayout, final int minimumLength, final String errorMessage) {
        CharSequence s = textInputLayout.getEditText().getText();
        if (s.length() > minimumLength) {
            textInputLayout.setError(null);
            textInputLayout.setErrorEnabled(false);
        } else {
            textInputLayout.setError(errorMessage);
            textInputLayout.setErrorEnabled(true);
        }
    }

    public static void checkLengthInTextInputLayout2(final TextInputLayout textInputLayout, final int minimumLength, final String errorMessage) {
        CharSequence s = textInputLayout.getEditText().getText();
        if (s.length() > minimumLength) {
            textInputLayout.setError(null);
            textInputLayout.setErrorEnabled(false);
        }
    }

    public static boolean checkPermission(Activity activity, String permission, int requestCode) {
        int permissionCheck = ContextCompat.checkSelfPermission(activity, permission);
        if (permissionCheck == PackageManager.PERMISSION_DENIED) {
            if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                    ActivityCompat.requestPermissions(activity, new String[]{permission}, requestCode);
                } else {
                    ActivityCompat.requestPermissions(activity, new String[]{permission}, requestCode);
                }
            }
        } else {
            return true;
        }
        return false;
    }

    public static void showLegal(Context context) {
        showOtherAppByUri(context, "http://diocian.com/legal/terms.view");
    }

    public static void showOtherAppByUri(Context context, String uriString) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        Uri u = Uri.parse(uriString);
        i.setData(u);
        context.startActivity(i);
    }

    public static void refreshFragment(final MainActivity activity) {
        final FragmentManager fragmentManager = activity.getSupportFragmentManager();
        final Fragment fragment = fragmentManager.findFragmentByTag(String.valueOf(fragmentManager.getBackStackEntryCount()-1));
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        Bundle bundle = fragment.getArguments();
        transaction.remove(fragment);
        transaction.commit();
        fragmentManager.popBackStack();

        Handler mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {

                FragmentManager fragmentManager = activity.getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.fragment_container, fragment, String.valueOf(fragmentManager.getBackStackEntryCount()));
                transaction.addToBackStack(null);
                transaction.commit();
            }
        }, 500);

    }

    public static void replaceFragment(MainActivity activity, Fragment fragment, Bundle bundleParam) {
        replaceFragment(activity, fragment, bundleParam, true);
    }

    public static void replaceFragment(final MainActivity activity, final Fragment fragment, Bundle bundleParam, final boolean isAddToBackStack) {
        activity.hidePlaylists();
        fragment.setArguments(bundleParam);
        final FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        String bundleString = bundleParam == null ? "" : bundleParam.toString();
        int entry = fragmentManager.getBackStackEntryCount() > 0 ? fragmentManager.getBackStackEntryCount() - 1 : 0;
        if (fragmentManager.getBackStackEntryCount() > 0) {
            Fragment prevFragment = fragmentManager.findFragmentByTag(String.valueOf(entry));
            if (prevFragment != null) {
                Bundle prevBundle = prevFragment.getArguments();
                String prevBundleString = prevBundle == null ? "" : prevBundle.toString();
                if (fragment.getClass() == prevFragment.getClass() && bundleString.equals(prevBundleString)) {
                    transaction.remove(fragmentManager.findFragmentByTag(String.valueOf(entry)));
                    transaction.commit();
                    fragmentManager.popBackStack();
                }
            }
        }

        Handler mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                FragmentManager fragmentManager = activity.getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.fragment_container, fragment, String.valueOf(fragmentManager.getBackStackEntryCount()));
                if (isAddToBackStack)
                    transaction.addToBackStack(null);
                transaction.commit();
            }
        }, 500);

    }

    public static void getBackStackFragments(MainActivity activity) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        for (int entry = 0; entry < fragmentManager.getBackStackEntryCount(); entry++) {
            Log.d(GlobalConstants.LOG_TAG, GlobalConstants.LOG_PREFIX + "BackStack Fragment " + entry + " : " + fragmentManager.findFragmentByTag(String.valueOf(entry)));
        }
    }

    public static void removeAndReplaceFragment(MainActivity activity, Fragment currentFragment, Fragment toFragment, Bundle bundleParam) {
        activity.hidePlaylists();
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.remove(currentFragment);
        fragmentManager.popBackStack();
        toFragment.setArguments(bundleParam);
        transaction.replace(R.id.fragment_container, toFragment);
        transaction.addToBackStack(String.valueOf(fragmentManager.getBackStackEntryCount()));
        transaction.commit();
    }

    public static void removeAndReplaceFragment2(MainActivity activity, Fragment currentFragment, Fragment toFragment, Bundle bundleParam) {
        activity.hidePlaylists();
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.remove(currentFragment);
        fragmentManager.popBackStack();
        toFragment.setArguments(bundleParam);
        transaction.replace(R.id.fragment_container, toFragment);
        transaction.addToBackStack(null);
        transaction.commit();
        Log.d(GlobalConstants.LOG_TAG, GlobalConstants.LOG_PREFIX + "removeAndReplaceFragment2 : " + String.valueOf(fragmentManager.getBackStackEntryCount()));
    }

    public static String encodeBase64FromUri(Activity activity, Uri uri) {
        try {
            int maxBufferSize = 2048;
            InputStream fileInputStream;

            fileInputStream = activity.getContentResolver().openInputStream(uri);

            if (fileInputStream != null) {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                int bytesAvailable = fileInputStream.available();
                int bufferSize = Math.min(bytesAvailable, maxBufferSize);
                byte[] buffer = new byte[bufferSize];
                int bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                while (bytesRead > 0) {
                    byteArrayOutputStream.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                }
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                return "data:image/jpeg;base64," + Base64.encodeToString(byteArray, Base64.DEFAULT);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String encodeUtf8(String param) {
        try {
            return URLEncoder.encode(param, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Uri rotateAndResizeImageFromUri(Activity activity, Uri srcUri, ImageView dstImageView, int reqWidth, int reqHeight) {
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(activity.getContentResolver(), srcUri);

            Matrix matrix = new Matrix();
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                if (DocumentsContract.isDocumentUri(activity, srcUri)) {
                    String wholeID = DocumentsContract.getDocumentId(srcUri);
                    String id = wholeID.split(":")[1];
                    String[] column = {MediaStore.Images.Media.DATA};
                    String sel = MediaStore.Images.Media._ID + "=?";
                    ContentResolver contentResolver = activity.getContentResolver();
                    Cursor cursor2 = contentResolver.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, column, sel, new String[]{id}, null);
                    String filePath = "";
                    int columnIndex = cursor2.getColumnIndex(column[0]);
                    if (cursor2.moveToFirst()) {
                        filePath = cursor2.getString(columnIndex);
                    }
                    cursor2.close();

                    ExifInterface exif = new ExifInterface(filePath);
                    int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                    int rotationInDegrees = exifToDegrees(rotation);
                    if (rotation != 0f) {
                        matrix.preRotate(rotationInDegrees);
                    }
                } else {
                    Cursor cursor2 = activity.getContentResolver().query(srcUri, null, null, null, null );
                    cursor2.moveToNext();
                    String path = cursor2.getString( cursor2.getColumnIndex( "_data" ) );
                    cursor2.close();

                    ExifInterface exif = new ExifInterface(path);
                    int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                    int rotationInDegrees = exifToDegrees(rotation);

                    Log.v(GlobalConstants.LOG_TAG, "rotation : " + rotation);
                    Log.v(GlobalConstants.LOG_TAG, "rotationInDegrees : " + rotationInDegrees);

                    if (rotation != 0f) {
                        matrix.preRotate(rotationInDegrees);
                    }
                }
            }

            int width = options.outWidth;
            reqWidth = reqWidth * 2;
            reqHeight = reqHeight * 2;
            int inSampleSize = 1;
            if (width > reqWidth) {
                while ((width / inSampleSize) > reqWidth) {
                    inSampleSize += 1;
                }
            }
            options.inSampleSize = inSampleSize;
            options.inJustDecodeBounds = false;
            Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, reqWidth, reqHeight, true);
            Bitmap rotatedBitmap = Bitmap.createBitmap(resizedBitmap, 0, 0, resizedBitmap.getWidth(), resizedBitmap.getHeight(), matrix, true);
            dstImageView.setImageBitmap(rotatedBitmap);

            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            String path = MediaStore.Images.Media.insertImage(activity.getContentResolver(), rotatedBitmap, "temp_images", null);
            return Uri.parse(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Uri rotateAndResizeImageFromUri2(Activity activity, Uri srcUri, ImageView dstImageView, int reqWidth) {

        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(activity.getContentResolver(), srcUri);

            Matrix matrix = new Matrix();

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {

                Log.v(GlobalConstants.LOG_TAG, "DocumentsContract.isDocumentUri(activity, srcUri) : " + DocumentsContract.isDocumentUri(activity, srcUri));

                if (DocumentsContract.isDocumentUri(activity, srcUri)) {
                    String wholeID = DocumentsContract.getDocumentId(srcUri);
                    String id = wholeID.split(":")[1];
                    String[] column = {MediaStore.Images.Media.DATA};
                    String sel = MediaStore.Images.Media._ID + "=?";
                    ContentResolver contentResolver = activity.getContentResolver();
                    Cursor cursor2 = contentResolver.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, column, sel, new String[]{id}, null);
                    String filePath = "";
                    int columnIndex = cursor2.getColumnIndex(column[0]);
                    if (cursor2.moveToFirst()) {
                        filePath = cursor2.getString(columnIndex);
                    }
                    cursor2.close();

                    ExifInterface exif = new ExifInterface(filePath);
                    int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                    int rotationInDegrees = exifToDegrees(rotation);

                    Log.v(GlobalConstants.LOG_TAG, "rotation : " + rotation);
                    Log.v(GlobalConstants.LOG_TAG, "rotationInDegrees : " + rotationInDegrees);

                    if (rotation != 0f) {
                        matrix.preRotate(rotationInDegrees);
                    }
                } else {
                    Cursor cursor2 = activity.getContentResolver().query(srcUri, null, null, null, null );
                    cursor2.moveToNext();
                    String path = cursor2.getString( cursor2.getColumnIndex( "_data" ) );
                    cursor2.close();

                    ExifInterface exif = new ExifInterface(path);
                    int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                    int rotationInDegrees = exifToDegrees(rotation);

                    Log.v(GlobalConstants.LOG_TAG, "rotation : " + rotation);
                    Log.v(GlobalConstants.LOG_TAG, "rotationInDegrees : " + rotationInDegrees);

                    if (rotation != 0f) {
                        matrix.preRotate(rotationInDegrees);
                    }
                }
            }

            int width2 = bitmap.getWidth();
            int height2 = bitmap.getHeight();
            int width = options.outWidth;
            reqWidth = reqWidth * 2;
            int reqHeight = reqWidth * height2 / width2;
            Log.d(GlobalConstants.LOG_TAG, "width: " + width2);
            Log.d(GlobalConstants.LOG_TAG, "height: " + height2);
            Log.d(GlobalConstants.LOG_TAG, "reqWidth: " + reqWidth);
            Log.d(GlobalConstants.LOG_TAG, "reqHeight: " + reqHeight);
            int inSampleSize = 1;
            if (width > reqWidth) {
                while ((width / inSampleSize) > reqWidth) {
                    inSampleSize += 1;
                }
            }
            options.inSampleSize = inSampleSize;
            options.inJustDecodeBounds = false;
            Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, reqWidth, reqHeight, true);
            Bitmap rotatedBitmap = Bitmap.createBitmap(resizedBitmap, 0, 0, resizedBitmap.getWidth(), resizedBitmap.getHeight(), matrix, true);
            dstImageView.setImageBitmap(rotatedBitmap);

            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            String path = MediaStore.Images.Media.insertImage(activity.getContentResolver(), rotatedBitmap, "temp_images", null);
            return Uri.parse(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static int exifToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270;
        }
        return 0;
    }

    public static ArrayList<View> getViewsByTag(ViewGroup root, String tag) {
        ArrayList<View> views = new ArrayList<>();
        if (root != null) {
            final int childCount = root.getChildCount();
            for (int i = 0; i < childCount; i++) {
                final View child = root.getChildAt(i);
                if (child instanceof ViewGroup) {
                    views.addAll(getViewsByTag((ViewGroup) child, tag));
                }
                final Object tagObj = child.getTag();
                if (tagObj != null && tagObj.equals(tag)) {
                    views.add(child);
                }
            }
        }
        return views;
    }

    public static void setBackgroundTintUnderLollipop(Context context, View view, int ResourceId) {
        final Drawable oDrawable = view.getBackground();
        final Drawable wDrawable = DrawableCompat.wrap(oDrawable);
        DrawableCompat.setTintList(wDrawable, ColorStateList.valueOf(ContextCompat.getColor(context, ResourceId)));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            view.setBackground(wDrawable);
        }
    }

    public static String formatCurrency(String value) {
        Pattern pattern = Pattern.compile("(^[+-]?\\d+)(\\d{3})");
        Matcher matcher = pattern.matcher(value);
        while (matcher.find()) {
            Log.d(GlobalConstants.LOG_TAG, "matcher.start() : " + matcher.start());
            Log.d(GlobalConstants.LOG_TAG, "matcher.end() : " + matcher.end());
            Log.d(GlobalConstants.LOG_TAG, "matcher.group(1) : " + matcher.group(1));
            Log.d(GlobalConstants.LOG_TAG, "matcher.group(1) : " + matcher.group(2));
            value = matcher.replaceAll("$1,$2");
            matcher.reset(value);
        }
        return "";
    }

    public static boolean isInteger(String value) {
        try {
            Integer.parseInt(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static String getFcmToken() {
        return FirebaseInstanceId.getInstance().getToken();
    }

    public static void showMenuNotificationCount(Menu menu, final Activity activity) {
        final MenuItem item = menu.findItem(R.id.action_notification);
        if (item != null) {
            if (((MainActivity) activity).getPreferences("member_sn") == null || ((MainActivity) activity).getPreferences("member_sn").equals("")) {
                item.setVisible(false);
                ShortcutBadger.removeCount(activity);
            } else {
                HttpRequest httpRequest = new HttpRequest(activity);
                HttpRequest.HttpGetRequestTask notificationCountHttpGetRequestTask = httpRequest.newHttpGetRequestTask(new HttpRequest.CallbackIf() {
                    @Override
                    public void callback(JSONObject jsonObject, Context context) {
                        try {
                            MenuItemCompat.setActionView(item, R.layout.menu_item_badge);
                            RelativeLayout notificationCountRelativeLayout = (RelativeLayout) MenuItemCompat.getActionView(item);
                            TextView tv = (TextView) notificationCountRelativeLayout.findViewById(R.id.actionbar_notifcation_textview);
                            tv.setText(jsonObject.getString("count"));
                            if (jsonObject.getString("count").equals("0")) {
                                tv.setVisibility(View.GONE);
                                ShortcutBadger.removeCount(activity);
                            } else {
                                ShortcutBadger.applyCount(activity, Integer.parseInt(jsonObject.getString("count")));
                            }
                            notificationCountRelativeLayout.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    MainActivity mainActivity = (MainActivity) activity;
                                    if (mainActivity != null) {
                                        mainActivity.viewNotifications();
                                    }
                                }
                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                notificationCountHttpGetRequestTask.execute(GlobalConstants.DOMAIN + "/getNotifiactionCountByMember.do?target_member_sn=" + ((MainActivity) activity).getPreferences("member_sn"));
            }
        }
    }

    public static void setNotificationCount(Context context, String count) {
        Log.v(GlobalConstants.LOG_TAG, "setNotificationCount : " + count);
    }

}
