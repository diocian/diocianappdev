package com.diocian.diocianappdev.util;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.OpenableColumns;
import android.util.Log;

import com.diocian.diocianappdev.MainActivity;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpRequestByMultipart {
    private Context context;

    public HttpRequestByMultipart(Context context) {
        this.context = context;
    }

    public JSONObject httpGetRequest(String url, Uri fileUri, Uri imageFileUri) {
        JSONObject jsonObject;
        try {
            URL target = new URL(url);
            HttpURLConnection httpURLConnection = (HttpURLConnection) target.openConnection();
            Log.d("TEST----->", "((MainActivity) context).getPreferences(\"session_id\") : " + ((MainActivity) context).getPreferences("session_id_raw"));
            httpURLConnection.setRequestProperty("Cookie", ((MainActivity) context).getPreferences("session_id_raw"));

            String boundary = "diocian_http_request";
            httpURLConnection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setChunkedStreamingMode(2048);

            DataOutputStream wr = new DataOutputStream(httpURLConnection.getOutputStream());

            wr.write(("\r\n--" + boundary + "\r\n").getBytes("UTF-8"));

            String mimeType = null;
            String fileName = "";
            ContentResolver contentResolver = context.getContentResolver();
            Cursor cursor = contentResolver.query(fileUri, null, null, null, null);
            Log.v(GlobalConstants.LOG_TAG, "cursor---->" + cursor);
            if (cursor != null) {
                int nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                cursor.moveToFirst();
                fileName = cursor.getString(nameIndex);
                nameIndex = cursor.getColumnIndexOrThrow("mime_type");
                cursor.moveToFirst();
                mimeType = cursor.getString(nameIndex);
                cursor.close();
            } else  {
                fileName = "diocian_recorded.mp4";
            }

            String fileType;
            if (mimeType != null) {
                fileType = mimeType.split("/")[0];
            } else {
                fileType = "audio";
            }
            Log.v(GlobalConstants.LOG_TAG, "Content-Disposition: form-data; name=\"" + fileType + "\"; filename=\"" + fileName);
            Log.v(GlobalConstants.LOG_TAG, "Content-Disposition: form-data; name=\"" + fileType + "\"; filename=\"" + fileName);
            Log.v(GlobalConstants.LOG_TAG, "Content-Disposition: form-data; name=\"" + fileType + "\"; filename=\"" + fileName);
            wr.write(("Content-Disposition: form-data; name=\"" + fileType + "\"; filename=\"" + fileName + "\"\r\n").getBytes("UTF-8"));
            wr.write(("Content-Type: application/octet-stream\r\n\r\n").getBytes("UTF-8"));

            int maxBufferSize = 2048;
            InputStream fileInputStream = context.getContentResolver().openInputStream(fileUri);
            if (fileInputStream != null) {
                int bytesAvailable = fileInputStream.available();
                int bufferSize = Math.min(bytesAvailable, maxBufferSize);
                byte[] buffer = new byte[bufferSize];
                int bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                while (bytesRead > 0) {
                    DataOutputStream dataWrite = new DataOutputStream(httpURLConnection.getOutputStream());
                    dataWrite.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                }
            }

            if (imageFileUri != null) {
                wr.write(("\r\n--" + boundary + "\r\n").getBytes("UTF-8"));
                mimeType = null;
                fileName = "";
                contentResolver = context.getContentResolver();
                cursor = contentResolver.query(imageFileUri, null, null, null, null);
                if (cursor != null) {
                    int nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                    cursor.moveToFirst();
                    fileName = cursor.getString(nameIndex);
                    nameIndex = cursor.getColumnIndexOrThrow("mime_type");
                    cursor.moveToFirst();
                    mimeType = cursor.getString(nameIndex);
                    cursor.close();
                }

                if (mimeType != null) {
                    fileType = mimeType.split("/")[0];
                }

                wr.write(("Content-Disposition: form-data; name=\"" + fileType + "\"; filename=\"" + fileName + "\"\r\n").getBytes("UTF-8"));
                wr.write(("Content-Type: application/octet-stream\r\n\r\n").getBytes("UTF-8"));

                fileInputStream = context.getContentResolver().openInputStream(imageFileUri);
                if (fileInputStream != null) {
                    int bytesAvailable = fileInputStream.available();
                    int bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    byte[] buffer = new byte[bufferSize];
                    int bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                    while (bytesRead > 0) {
                        DataOutputStream dataWrite = new DataOutputStream(httpURLConnection.getOutputStream());
                        dataWrite.write(buffer, 0, bufferSize);
                        bytesAvailable = fileInputStream.available();
                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                    }
                    fileInputStream.close();
                }

            }

            wr.write(("\r\n--" + boundary + "--\r\n").getBytes("UTF-8"));
            wr.flush();
            wr.close();

            String headerSession = httpURLConnection.getHeaderField("Set-Cookie");
            InputStream inputStream = new BufferedInputStream(httpURLConnection.getInputStream());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            StringBuilder stringBuilder = new StringBuilder();
            String input;
            while ((input = bufferedReader.readLine()) != null)
                stringBuilder.append(input);
            jsonObject = new JSONObject(stringBuilder.toString());
            if (headerSession == null) headerSession = "";
            jsonObject.put("session_id", headerSession);

            bufferedReader.close();
            inputStream.close();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return jsonObject;
    }

    public HttpGetRequestTask newHttpGetRequestTask(CallbackIf successCallback, Uri fileUri, Uri imageFileUri) {
        return new HttpGetRequestTask(successCallback, fileUri, imageFileUri);
    }

    public class HttpGetRequestTask extends AsyncTask<String, Void, JSONObject> {

        private CallbackIf successCallback;
        private Uri fileUri;
        private Uri imageFileUri;

        public HttpGetRequestTask(CallbackIf successCallback, Uri fileUri, Uri imageFileUri) {
            this.successCallback = successCallback;
            this.fileUri = fileUri;
            this.imageFileUri = imageFileUri;
        }

        @Override
        protected JSONObject doInBackground(String... url) {
            return httpGetRequest(url[0], fileUri, imageFileUri);
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            successCallback.callback(jsonObject, context);
        }
    }

    public interface CallbackIf extends Serializable {
        void callback(JSONObject jsonObject, Context context);
    }

}
