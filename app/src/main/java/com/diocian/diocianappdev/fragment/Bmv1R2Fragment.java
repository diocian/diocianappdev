package com.diocian.diocianappdev.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.adapter.MusicRecyclerAdapter;
import com.diocian.diocianappdev.listener.EndlessRecyclerViewScrollListener;
import com.diocian.diocianappdev.model.Post;
import com.diocian.diocianappdev.util.GlobalConstants;
import com.diocian.diocianappdev.util.HttpRequest;
import com.diocian.diocianappdev.util.ImageFetcher;
import com.diocian.diocianappdev.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Bmv1R2Fragment extends Fragment {

    private ImageFetcher imageFetcher;
    private RecyclerView musicRecyclerView;
    private ArrayList<Post> musics = new ArrayList<>();
    private MusicRecyclerAdapter musicRecyclerAdapter;
    private StaggeredGridLayoutManager musicLayoutManager;

    private int get_start_no = 0;
    private boolean loading = false;

    private boolean isEnd = false;

    public Bmv1R2Fragment() {
    }

    public ArrayList<Post> getMusics() {
        return musics;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        System.gc();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        imageFetcher = new ImageFetcher(getContext());
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        musicLayoutManager.setSpanCount(Utils.calColumnCount(getContext()));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_bmv1_r2, container, false);
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.app_bar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }

        ImageView imageViewCover = (ImageView) view.findViewById(R.id.imageViewCover);
        imageFetcher.setImageToImageView(GlobalConstants.DOMAIN + "/images/image_bmv1_cover_"+getResources().getString(R.string.lang_cd)+"_m.jpg", imageViewCover, 1000);

        musicRecyclerView = (RecyclerView) view.findViewById(R.id.bmv1_r2_recyclerView);
        musicLayoutManager = new StaggeredGridLayoutManager(Utils.calColumnCount(getContext()), StaggeredGridLayoutManager.VERTICAL);
        musicRecyclerView.setLayoutManager(musicLayoutManager);
        musicRecyclerAdapter = new MusicRecyclerAdapter(musics, imageFetcher, getContext());
        if (isEnd) {
            musicRecyclerAdapter.removeFooter();
        }
        musicRecyclerView.setAdapter(musicRecyclerAdapter);
        musicRecyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(musicLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                loadMorePosts();
            }
        });

        if (musics.size() == 0) {
            loadMorePosts();
        }

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_competition, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menuItemYoutube) {
            Intent i = new Intent(Intent.ACTION_VIEW);
            Uri u = Uri.parse("https://www.youtube.com/channel/UC1zo0v3_GHwKRUM5cF3yGpg");
            i.setData(u);
            startActivity(i);
            return true;
        } else if (id == R.id.menuItemFacebook) {
            String lang_cd = getResources().getString(R.string.lang_cd);
            String uriString = "https://www.facebook.com/diocianglobal/";
            if (lang_cd.equals("jp")) {
                uriString = "https://www.facebook.com/diocianjapan/";
            } else if (lang_cd.equals("kr")) {
                uriString = "https://www.facebook.com/diociankorea/";
            } else if (lang_cd.equals("vn")) {
                uriString = "https://www.facebook.com/diocianvietnam/";
            }
            Intent i = new Intent(Intent.ACTION_VIEW);
            Uri u = Uri.parse(uriString);
            i.setData(u);
            startActivity(i);
            return true;
        } else if (id == android.R.id.home) {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            fragmentManager.popBackStackImmediate();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void loadMorePosts() {
        if (!loading && !isEnd) {
            loading = true;
            Map<String, String> parameterMap = new HashMap<>();
            String target_dc = "entry";
            String parent_sn = "7181";
            parameterMap.put("target_dc", target_dc);
            parameterMap.put("parent_sn", parent_sn);
            parameterMap.put("get_start_no", String.valueOf(get_start_no));
            int get_count = 12;
            parameterMap.put("get_count", String.valueOf(get_count));
            String url = GlobalConstants.DOMAIN + "/getPosts.do?";
            for (String key : parameterMap.keySet()) {
                url += key + "=" + parameterMap.get(key) + "&";
            }
            HttpRequest httpRequest = new HttpRequest(getContext());
            HttpRequest.HttpGetRequestTask musicsHttpGetRequestTask = httpRequest.newHttpGetRequestTask(new HttpRequest.CallbackIf() {
                @Override
                public void callback(JSONObject jsonObject, Context context) {
                    try {
                        int postCount = jsonObject.getInt("postCount");
                        JSONArray posts = jsonObject.getJSONArray("posts");
                        JSONArray images = jsonObject.getJSONArray("images");
                        JSONObject post, image;
                        String imageUrl;
                        for (int i = 0; i < posts.length(); i++) {
                            post = posts.getJSONObject(i);
                            imageUrl = null;
                            for (int j = 0; j < images.length(); j++) {
                                image = images.getJSONObject(j);
                                if (post.getString("post_sn").equals(image.getString("target_sn"))) {
                                    imageUrl = GlobalConstants.DOMAIN + image.getString("file_url");
                                    break;
                                }
                            }
                            musics.add(new Post().setMusic(post.getString("post_sn"), post.getString("title"), post.getString("artist_name"), imageUrl, post.getString("genre_name"), post.getString("like_sn"), post.getString("comment_sn"), post.getString("member_name"), GlobalConstants.DOMAIN + post.getString("member_image_url"), GlobalConstants.DOMAIN + post.getString("audio_file_url"), post.getString("creator_sn"), post.getString("my_like_count"), post.getString("follow_sn")));
                            musicRecyclerAdapter.notifyItemInserted(get_start_no + 1 + i);
                        }

                        get_start_no = get_start_no + posts.length();
                        if (musics.size() < postCount) {
                            loading = false;
                        } else {
                            Handler mHandler = new Handler();
                            mHandler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    musicRecyclerAdapter.removeFooter();
                                    isEnd = true;
                                    musicRecyclerAdapter.notifyItemRemoved(get_start_no + 2);
                                }
                            }, 500);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
            musicsHttpGetRequestTask.execute(url);
        }
    }

}
