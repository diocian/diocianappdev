package com.diocian.diocianappdev.fragment;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.diocian.diocianappdev.MainActivity;
import com.diocian.diocianappdev.listener.EndlessRecyclerViewScrollListener;
import com.diocian.diocianappdev.model.Post;
import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.adapter.VideoRecyclerAdapter;
import com.diocian.diocianappdev.util.GlobalConstants;
import com.diocian.diocianappdev.util.HttpRequest;
import com.diocian.diocianappdev.util.ImageFetcher;
import com.diocian.diocianappdev.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class VideosFragment extends Fragment {
    private RecyclerView videoRecyclerView;
    private ArrayList<Post> videos = new ArrayList<>();
    private VideoRecyclerAdapter videoRecyclerAdapter;
    private ImageFetcher imageFetcher;
    private StaggeredGridLayoutManager videoLayoutManager;

    private int get_start_no = 0;
    private boolean loading = false;

    private boolean isEnd = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imageFetcher = new ImageFetcher(getActivity());
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        videoLayoutManager.setSpanCount(Utils.calColumnCount(getContext()));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        videoRecyclerView.removeAllViews();
//        videoRecyclerView = null;
//        System.gc();
    }

    String search_word;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_videos, container, false);

        search_word = null;
        MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity != null) {
            search_word = mainActivity.query;
        }
        if (search_word == null) {
            search_word = "";
        }

        videoRecyclerView = (RecyclerView) view.findViewById(R.id.videos_recyclerView);
        videoLayoutManager = new StaggeredGridLayoutManager(Utils.calColumnCount(getContext()), StaggeredGridLayoutManager.VERTICAL);
        videoRecyclerView.setLayoutManager(videoLayoutManager);
        videoRecyclerAdapter = new VideoRecyclerAdapter(videos, imageFetcher, getContext());
        if (isEnd) {
            videoRecyclerAdapter.removeFooter();
        }
        videoRecyclerView.setAdapter(videoRecyclerAdapter);
        videoRecyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(videoLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                loadMorePosts();
            }
        });

        if (videos.size() == 0) {
            loadMorePosts();
        }

        return view;
    }

    public void loadMorePosts() {
        if (!loading && !isEnd) {
            loading = true;
            Map<String, String> parameterMap = new HashMap<>();
            String target_dc = "video";
            parameterMap.put("target_dc", target_dc);
            parameterMap.put("get_start_no", String.valueOf(get_start_no));
            int get_count = 12;
            parameterMap.put("get_count", String.valueOf(get_count));
            parameterMap.put("search_word", search_word);
            String url = GlobalConstants.DOMAIN + "/getPosts.do?";
            for (String key : parameterMap.keySet()) {
                url += key + "=" + parameterMap.get(key) + "&";
            }
            HttpRequest httpRequest = new HttpRequest(getContext());
            HttpRequest.HttpGetRequestTask studiosHttpGetRequestTask = httpRequest.newHttpGetRequestTask(new HttpRequest.CallbackIf() {
                @Override
                public void callback(JSONObject jsonObject, Context context) {
                    try {
                        int postCount = jsonObject.getInt("postCount");
                        JSONArray posts = jsonObject.getJSONArray("posts");
                        JSONObject post;
                        for (int i = 0; i < posts.length(); i++) {
                            post = posts.getJSONObject(i);
                            videos.add(new Post().setVideo(post.getString("youtube_video_id"), post.getString("post_sn"), post.getString("genre_name"), "http://img.youtube.com/vi/" + post.getString("youtube_video_id") + "/0.jpg", post.getString("title"), post.getString("member_name"), post.getString("like_sn"), post.getString("comment_sn"), Utils.getTimeLag(post.getString("created_datetime").substring(0, 19), getContext()), GlobalConstants.DOMAIN + post.getString("member_image_url"), post.getString("my_like_count"), post.getString("creator_sn"), post.getString("follow_sn"), post.getString("audio_file_url")));
                            videoRecyclerAdapter.notifyItemInserted(get_start_no + 1 + i);
                        }
                        get_start_no = get_start_no + posts.length();
                        if (videos.size() < postCount) {
                            loading = false;
                        } else {
                            Handler mHandler = new Handler();
                            mHandler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    videoRecyclerAdapter.removeFooter();
                                    isEnd = true;
                                    videoRecyclerAdapter.notifyItemRemoved(get_start_no + 2);
                                }
                            }, 500);
                        }
                    } catch (JSONException | ParseException e) {
                        e.printStackTrace();
                    }
                }
            });
            studiosHttpGetRequestTask.execute(url);
        }
    }

}
