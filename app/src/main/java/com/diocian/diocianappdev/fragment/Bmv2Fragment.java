package com.diocian.diocianappdev.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.diocian.diocianappdev.MainActivity;
import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.adapter.ListPagerAdapter;
import com.diocian.diocianappdev.util.GlobalConstants;
import com.diocian.diocianappdev.util.ImageFetcher;
import com.diocian.diocianappdev.util.Utils;

public class Bmv2Fragment extends Fragment {

    private ImageFetcher imageFetcher;

    private CharSequence[] tabTitles = {
            "Home",
            "Entries",
            "Info",
            "Help"
    };

    private TabLayout tabLayout;
    ViewPager mViewPager;

    public Bmv2Fragment() {
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        System.gc();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        imageFetcher = new ImageFetcher(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_bmv1, container, false);
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.app_bar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fabApplication);
        fab.setVisibility(View.GONE);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).isLogined(true, new MainActivity.Callback_isLoggedIn(){
                    @Override
                    public void callback(Context context, boolean isLoggedIn) {
                        if (isLoggedIn) {
                            Utils.replaceFragment((MainActivity) getActivity(), new Bmv2ApplyFragment(), new Bundle());
                        }
                    }
                });
            }
        });

        ImageView imageViewCover = (ImageView) view.findViewById(R.id.imageViewCover);
        imageFetcher.setImageToImageView(GlobalConstants.DOMAIN + "/bmv2-1/images/image_cover_m_"+getResources().getString(R.string.lang_cd2)+".jpg", imageViewCover, 1000);

        // TODO : B 오디션 순위

        ListPagerAdapter mSectionsPagerAdapter = new ListPagerAdapter(getChildFragmentManager());
        mViewPager = (ViewPager) view.findViewById(R.id.container);
        mSectionsPagerAdapter.addFragment(new Bmv2HomeFragment(), tabTitles[0].toString());

        String member_sn = ((MainActivity) getActivity()).getPreferences("member_sn");
//        if(!member_sn.equals("")) {
            mSectionsPagerAdapter.addFragment(new Bmv2EntriesFragment(), tabTitles[1].toString());
//        }
        mSectionsPagerAdapter.addFragment(new Bmv2InfoFragment(), tabTitles[2].toString());

        mViewPager.setAdapter(mSectionsPagerAdapter);

        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
//        for (int i = 0; i < tabLayout.getTabCount(); i++) {
//            tabLayout.getTabAt(i).setText(tabTitles[i]);
//        }
        tabLayout.getTabAt(0).setText(tabTitles[0]);
//        if(!member_sn.equals("")) {
            tabLayout.getTabAt(1).setText(tabTitles[1]);
            tabLayout.getTabAt(2).setText(tabTitles[2]);
//        } else {
//            tabLayout.getTabAt(1).setText(tabTitles[2]);
//        }


        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager) {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                super.onTabSelected(tab);
            }
        });

        Bundle bundle = this.getArguments();
        int tabNo = 0;
        if (bundle != null) {
            tabNo = bundle.getInt("tabNo");
        }
        setSelectedTab(tabNo);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_competition, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menuItemYoutube) {
            Intent i = new Intent(Intent.ACTION_VIEW);
            Uri u = Uri.parse("https://www.youtube.com/channel/UC1zo0v3_GHwKRUM5cF3yGpg");
            i.setData(u);
            startActivity(i);
            return true;
        } else if (id == R.id.menuItemFacebook) {
            String lang_cd = getResources().getString(R.string.lang_cd);
            String uriString = "https://www.facebook.com/diocianglobal/";
            if (lang_cd.equals("jp")) {
                uriString = "https://www.facebook.com/diocianjapan/";
            } else if (lang_cd.equals("kr")) {
                uriString = "https://www.facebook.com/diociankorea/";
            } else if (lang_cd.equals("vn")) {
                uriString = "https://www.facebook.com/diocianvietnam/";
            }
            Intent i = new Intent(Intent.ACTION_VIEW);
            Uri u = Uri.parse(uriString);
            i.setData(u);
            startActivity(i);
            return true;
        } else if (id == android.R.id.home) {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            fragmentManager.popBackStackImmediate();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void setSelectedTab(int tabNo) {
        TabLayout.Tab selectedTab = tabLayout.getTabAt(tabNo);
        if (selectedTab != null) {
            selectedTab.select();
        }
    }

}
