package com.diocian.diocianappdev.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;

import com.diocian.diocianappdev.MainActivity;
import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.util.GlobalConstants;
import com.diocian.diocianappdev.util.HttpRequest;
import com.diocian.diocianappdev.util.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class CloudPaymentFragment extends Fragment {

    private static final String target_url_prefix = "www.diocian.com";
    private Context context;
    private WebView webViewFacebookLoginPop;
    private FrameLayout frameLayoutCloudPayment;
    private String aid;
    private String am;
    private String target_dc;
    private String target_sn;
    private String post_target_dc;

    View view;

    public CloudPaymentFragment() {
    }

    @SuppressLint({"SetJavaScriptEnabled", "AddJavascriptInterface"})
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_cloudpayment, container, false);

        Bundle bundle = this.getArguments();
        aid = "";
        am = "";
        target_dc = "";
        target_sn = "";
        post_target_dc = "";
        if (bundle != null) {
            aid = bundle.getString("aid");
            am = bundle.getString("am");
            target_dc = bundle.getString("target_dc");
            target_sn = bundle.getString("target_sn");
            post_target_dc = bundle.getString("post_target_dc");
        }

        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.setAcceptCookie(true);

        frameLayoutCloudPayment = (FrameLayout) view.findViewById(R.id.frameLayoutCloudPayment);
        WebView webViewCloudPayment = (WebView) view.findViewById(R.id.webViewCloudPayment);
        if (webViewCloudPayment != null) {
            webViewCloudPayment.clearCache(true);
            webViewCloudPayment.clearHistory();
            clearCookies(getActivity());
            WebSettings webSettingsFacebookLogin = webViewCloudPayment.getSettings();
            webSettingsFacebookLogin.setJavaScriptEnabled(true);
            webSettingsFacebookLogin.setAppCacheEnabled(true);
            webSettingsFacebookLogin.setJavaScriptCanOpenWindowsAutomatically(true);
            webSettingsFacebookLogin.setSupportMultipleWindows(true);

            webViewCloudPayment.setWebViewClient(new FacebookWebViewClient());
            webViewCloudPayment.setWebChromeClient(new FacebookWebChromeClient());
            String cloudPaymentUrl = "/app/payment/cloudpayment/form.jsp?aid=" + aid + "&am=" + am + "&formtype=Android&target_dc=" + target_dc + "&target_sn=" + target_sn + "&post_target_dc=" + post_target_dc + "&creator_sn=" + ((MainActivity) getActivity()).getPreferences("member_sn");
//            String cloudPaymentUrl = "/app/payment/cloudpayment/result.jsp?target_dc="+post_target_dc+"&target_sn="+target_sn;
            webViewCloudPayment.loadUrl(GlobalConstants.DOMAIN + cloudPaymentUrl);
        }
        context = getActivity();

        return view;
    }

    @SuppressWarnings("deprecation")
    public static void clearCookies(Context context) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();
        } else {
            CookieSyncManager cookieSyncMngr = CookieSyncManager.createInstance(context);
            cookieSyncMngr.startSync();
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.removeAllCookie();
            cookieManager.removeSessionCookie();
            cookieSyncMngr.stopSync();
            cookieSyncMngr.sync();
        }
    }

    private class FacebookWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            String host = Uri.parse(url).getHost();
            if (host.equals(target_url_prefix)) {
                if (webViewFacebookLoginPop != null) {
                    webViewFacebookLoginPop.setVisibility(View.GONE);
                    frameLayoutCloudPayment.removeView(webViewFacebookLoginPop);
                    webViewFacebookLoginPop = null;
                }
                return false;
            }
            if (host.equals("m.facebook.com") || host.equals("www.facebook.com")) {
                return false;
            }
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);
            return true;
        }

        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            //super.onReceivedSslError(view, handler, error);
        }
    }

    class FacebookWebChromeClient extends WebChromeClient {

        @SuppressLint("SetJavaScriptEnabled")
        @Override
        public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
            webViewFacebookLoginPop = new WebView(context);
            webViewFacebookLoginPop.setVerticalScrollBarEnabled(false);
            webViewFacebookLoginPop.setHorizontalScrollBarEnabled(false);

            CloudPaymentJavascriptInterface facebookLoginJavascriptInterface = new CloudPaymentJavascriptInterface(getActivity(), webViewFacebookLoginPop);
            webViewFacebookLoginPop.addJavascriptInterface(facebookLoginJavascriptInterface, "DiocianApp");

            webViewFacebookLoginPop.setWebViewClient(new FacebookWebViewClient());
            webViewFacebookLoginPop.getSettings().setJavaScriptEnabled(true);
            webViewFacebookLoginPop.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            frameLayoutCloudPayment.addView(webViewFacebookLoginPop);
            WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
            transport.setWebView(webViewFacebookLoginPop);
            resultMsg.sendToTarget();
            return true;
        }

        @Override
        public void onCloseWindow(WebView window) {
            Log.d("onCloseWindow", "called");
        }

    }

    public class CloudPaymentJavascriptInterface {

        private Activity activity;
        private WebView webView;

        public CloudPaymentJavascriptInterface(Activity activity, WebView webView) {
            this.activity = activity;
            this.webView = webView;
        }

        @JavascriptInterface
        public void cloudPaymentCallback(String target_dc, String target_sn) throws JSONException {
//            Snackbar.make(webView, jobCode + " : " + message, Snackbar.LENGTH_LONG).setAction("Action", null).show();
            Log.d(target_dc, target_sn);
            if (target_dc.equals("music")) {
//                ((MainActivity) getActivity()).viewMusic(target_sn);
                Bundle bundle = new Bundle();
                bundle.putString("post_sn", target_sn);
                bundle.putString("buyYn", "Y");
                Utils.removeAndReplaceFragment((MainActivity) getActivity(), new CloudPaymentFragment(), new MusicFragment(), bundle);
            } else if (target_dc.equals("market")) {
                Bundle bundle = new Bundle();
                bundle.putString("post_sn", target_sn);
                bundle.putString("buyYn", "Y");
                Utils.removeAndReplaceFragment((MainActivity) getActivity(), new CloudPaymentFragment(), new MarketFragment(), bundle);
            }
        }

        @JavascriptInterface
        public void showMessageToSnackbar(String message) {
//            Snackbar.make(webView, message, Snackbar.LENGTH_LONG).setAction("Action", null).show();

        }

    }

}