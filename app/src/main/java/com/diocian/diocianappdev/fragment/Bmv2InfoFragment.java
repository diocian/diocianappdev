package com.diocian.diocianappdev.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.util.GlobalConstants;
import com.diocian.diocianappdev.util.ImageFetcher;

public class Bmv2InfoFragment extends Fragment {

    private ImageFetcher imageFetcher;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imageFetcher = new ImageFetcher(getActivity());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        System.gc();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bmv1_info, container, false);

        ImageView imageViewPoster = (ImageView) view.findViewById(R.id.imageViewPoster);
        String lang_cd = getResources().getString(R.string.lang_cd2);
        String imageUrl = GlobalConstants.DOMAIN + "/bmv2-1/images/image_bmv2_" + lang_cd + ".jpg";
        imageFetcher.setImageToImageView(imageUrl, imageViewPoster, 324);

        return view;
    }

}
