package com.diocian.diocianappdev.fragment;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.diocian.diocianappdev.MainActivity;
import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.adapter.ListPagerAdapter;
import com.diocian.diocianappdev.util.GlobalConstants;
import com.diocian.diocianappdev.util.HttpRequest;
import com.diocian.diocianappdev.util.ImageFetcher;
import com.diocian.diocianappdev.util.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MemberFragment extends Fragment {

    private View view;
    private ImageFetcher imageFetcher;

    private ImageButton imageViewFollow;
    private int myFollowCount = 0;

    String member_sn = "";
    TabLayout tabLayout;
    int tabNo = 0;

    ViewPager mViewPager;

    private String[] tabTitles = {
            "Feeds",
            "Followers",
            "Followings",
            "Friends"
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        imageFetcher = new ImageFetcher(getActivity());
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        System.gc();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_member, container, false);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.detail_toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        final FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab_post);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).isLogined(true, new MainActivity.Callback_isLoggedIn() {
                    @Override
                    public void callback(Context context, boolean isLoggedIn) {
                        if (isLoggedIn) {
                            Bundle bundle = new Bundle();
                            bundle.putString("member_sn", member_sn);
                            Utils.replaceFragment((MainActivity) getActivity(), new PostFragment(), bundle);
//                            Utils.getBackStackFragments((MainActivity) getActivity());
                        }
                    }
                });
            }
        });

        final Bundle bundle = this.getArguments();

        if (bundle != null) {
            member_sn = bundle.getString("member_sn");
            tabNo = bundle.getInt("tabNo");
        }

        ((MainActivity) getActivity()).isLogined(false, new MainActivity.Callback_isLoggedIn() {
            @Override
            public void callback(Context context, boolean isLoggedIn) {
                if (isLoggedIn && ((MainActivity) getActivity()).getPreferences("member_sn").equals(member_sn)) {
                    fab.setVisibility(View.VISIBLE);
                }
            }
        });

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }

        // TODO : Article 삭제 기능 추가.
        // TODO : Article 수정 기능 추가.

        imageViewFollow = (ImageButton) view.findViewById(R.id.imageViewFollow);
        imageViewFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).setFollow(member_sn, myFollowCount, new MemberFragment(), bundle);
            }
        });

        Map<String, String> parameterMap = new HashMap<>();
        parameterMap.put("member_sn", member_sn);
        String url = GlobalConstants.DOMAIN + "/getMember.do?";
        for (String key : parameterMap.keySet()) {
            url += key + "=" + parameterMap.get(key) + "&";
        }
        HttpRequest httpRequest = new HttpRequest(getContext());
        HttpRequest.HttpGetRequestTask musicHttpGetRequestTask = httpRequest.newHttpGetRequestTask(new HttpRequest.CallbackIf() {
            @Override
            public void callback(JSONObject jsonObject, Context context) {
                try {
                    JSONObject post = jsonObject.getJSONObject("member");

                    ImageView imageViewHomeImage = (ImageView) view.findViewById(R.id.imageViewHomeImage);
                    ImageView ImageViewMemberImage = (ImageView) view.findViewById(R.id.ImageViewMemberImage);
                    TextView textViewMemberName = (TextView) view.findViewById(R.id.textViewMemberName);
                    TextView textViewMemberSummary = (TextView) view.findViewById(R.id.textViewMemberSummary);
                    TextView textViewFeedCount = (TextView) view.findViewById(R.id.textViewFeedCount);
                    TextView textViewFollowerCount = (TextView) view.findViewById(R.id.textViewFollowerCount);

                    imageFetcher.setImageToImageView(GlobalConstants.DOMAIN + post.getString("home_image_url"), imageViewHomeImage, 360);
                    imageFetcher.setImageToImageView(GlobalConstants.DOMAIN + post.getString("member_image_url"), ImageViewMemberImage, 64);
                    textViewMemberName.setText(post.getString("member_name"));
                    textViewMemberSummary.setText(post.getString("member_summary"));
                    textViewFeedCount.setText(jsonObject.getString("postCount"));
                    textViewFollowerCount.setText(jsonObject.getString("followerCount"));
                    myFollowCount = Integer.parseInt(jsonObject.getString("myFollowCount"));

                    if (myFollowCount > 0) {
                        imageViewFollow.setColorFilter(ContextCompat.getColor(context, R.color.DIOCIAN));
                    } else {
                        imageViewFollow.setColorFilter(null);
                    }

                    if (member_sn.equals(((MainActivity) getActivity()).getPreferences("member_sn"))) {
                        ((MainActivity) getActivity()).savePreferences("member_image_url", post.getString("member_image_url"));
                        ((MainActivity) getActivity()).savePreferences("home_image_url", post.getString("home_image_url"));
                        ((MainActivity) getActivity()).setLoginInfo();
                    }

                    ListPagerAdapter mSectionsPagerAdapter = new ListPagerAdapter(getChildFragmentManager());
                    mViewPager = (ViewPager) view.findViewById(R.id.container);
                    Bundle bundle_param = new Bundle();
                    bundle_param.putString("member_sn", member_sn);
                    PostsFragment postsFragment = new PostsFragment();
                    postsFragment.setArguments(bundle_param);
                    mSectionsPagerAdapter.addFragment(postsFragment, "");

                    FollowersFragment followersFragment = new FollowersFragment();
                    bundle_param.putInt("followerCount", jsonObject.getInt("followerCount"));
                    followersFragment.setArguments(bundle_param);
                    mSectionsPagerAdapter.addFragment(followersFragment, "");

                    FollowingsFragment followingsFragment = new FollowingsFragment();
                    bundle_param.putInt("followingCount", jsonObject.getInt("followingCount"));
                    followingsFragment.setArguments(bundle_param);
                    mSectionsPagerAdapter.addFragment(followingsFragment, "");

                    FriendsFragment friendsFragment = new FriendsFragment();
                    bundle_param.putInt("friendCount", jsonObject.getInt("friendCount"));
                    friendsFragment.setArguments(bundle_param);
                    mSectionsPagerAdapter.addFragment(friendsFragment, "");

                    mViewPager.setAdapter(mSectionsPagerAdapter);

                    tabLayout = (TabLayout) view.findViewById(R.id.tabs);
                    tabLayout.setupWithViewPager(mViewPager);
                    for (int i = 0; i < tabLayout.getTabCount(); i++) {
                        tabLayout.getTabAt(i).setText(tabTitles[i]);
                    }

                    mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
                    tabLayout.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager) {
                        @Override
                        public void onTabSelected(TabLayout.Tab tab) {
                            super.onTabSelected(tab);
                            ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
                            if (actionBar != null) {
                                actionBar.setTitle(tabTitles[tab.getPosition()].toUpperCase());
                            }
                        }
                    });

                    setSelectedTab(tabNo);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        musicHttpGetRequestTask.execute(url);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_none, menu);
        Utils.showMenuNotificationCount(menu, getActivity());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_notification) {
            ((MainActivity) getActivity()).viewNotifications();
            return true;
        } else if (id == android.R.id.home) {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            fragmentManager.popBackStackImmediate();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void setSelectedTab(int tabNo) {
        TabLayout.Tab selectedTab = tabLayout.getTabAt(tabNo);
        if (selectedTab != null) {
            selectedTab.select();
        }
    }

}
