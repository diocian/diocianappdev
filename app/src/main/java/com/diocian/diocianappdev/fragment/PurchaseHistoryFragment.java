package com.diocian.diocianappdev.fragment;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.diocian.diocianappdev.MainActivity;
import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.adapter.PurchaseRecyclerAdapter;
import com.diocian.diocianappdev.listener.EndlessRecyclerViewScrollListener;
import com.diocian.diocianappdev.model.Purchase;
import com.diocian.diocianappdev.util.GlobalConstants;
import com.diocian.diocianappdev.util.HttpRequest;
import com.diocian.diocianappdev.util.ImageFetcher;
import com.diocian.diocianappdev.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PurchaseHistoryFragment extends Fragment {
    private RecyclerView recyclerViewPurchaseHistory;
    private ArrayList<Purchase> purchases = new ArrayList<>();
    private PurchaseRecyclerAdapter purchaseRecyclerAdapter;
    private ImageFetcher imageFetcher;
    private StaggeredGridLayoutManager videoLayoutManager;

    private int get_start_no = 0;
    private boolean loading = false;

    private boolean isEnd = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imageFetcher = new ImageFetcher(getActivity());
        setHasOptionsMenu(true);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        videoLayoutManager.setSpanCount(1);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        recyclerViewPurchaseHistory.removeAllViews();
//        recyclerViewPurchaseHistory = null;
//        System.gc();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_purchase_history, container, false);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.app_bar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        String toolbar_title = getString(R.string.purchase_history).toUpperCase();
        toolbar.setTitle(toolbar_title);

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        recyclerViewPurchaseHistory = (RecyclerView) view.findViewById(R.id.recyclerViewPurchaseHistory);
        videoLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        recyclerViewPurchaseHistory.setLayoutManager(videoLayoutManager);
        purchaseRecyclerAdapter = new PurchaseRecyclerAdapter(purchases, imageFetcher, getContext());
        if (isEnd) {
            purchaseRecyclerAdapter.removeFooter();
        }
        recyclerViewPurchaseHistory.setAdapter(purchaseRecyclerAdapter);
        recyclerViewPurchaseHistory.addOnScrollListener(new EndlessRecyclerViewScrollListener(videoLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                loadMorePosts();
            }
        });

        if (purchases.size() == 0) {
            loadMorePosts();
        }

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_none, menu);
        Utils.showMenuNotificationCount(menu, getActivity());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_notification) {
            ((MainActivity) getActivity()).viewNotifications();
            return true;
        } else if (id == android.R.id.home) {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            fragmentManager.popBackStackImmediate();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void loadMorePosts() {
        if (!loading && !isEnd) {
            loading = true;
            Map<String, String> parameterMap = new HashMap<>();
            int get_count = 1000;
            parameterMap.put("get_count", String.valueOf(get_count));
            String url = GlobalConstants.DOMAIN + "/getPurchaseHistory.do?";
            for (String key : parameterMap.keySet()) {
                url += key + "=" + parameterMap.get(key) + "&";
            }
            HttpRequest httpRequest = new HttpRequest(getContext());
            HttpRequest.HttpGetRequestTask studiosHttpGetRequestTask = httpRequest.newHttpGetRequestTask(new HttpRequest.CallbackIf() {
                @Override
                public void callback(JSONObject jsonObject, Context context) {
                    try {
//                        int postCount = jsonObject.getInt("postCount");
                        JSONArray purchaseHistories = jsonObject.getJSONArray("purchaseHistories");
                        JSONObject purchase;
                        for (int i = 0; i < purchaseHistories.length(); i++) {
                            purchase = purchaseHistories.getJSONObject(i);
                            String target_dc = purchase.getString("target_dc");
                            String post_title = "";
                            String seller_name = "";
                            if (target_dc.equals("post") || target_dc.equals("market") || target_dc.equals("music")) {
                                target_dc = purchase.getString("post_target_dc").toUpperCase();
                                post_title = purchase.getString("post_title");
                                seller_name = purchase.getString("post_owner_member_name");
                            }else{
                                target_dc = purchase.getString("target_dc").toUpperCase();
                                if(purchase.getString("target_sn").equals("1")) {
                                    post_title = "Pro";
                                } else if(purchase.getString("target_sn").equals("2")) {
                                    post_title = "Plus";
                                } else if(purchase.getString("target_sn").equals("3")) {
                                    post_title = "Unlimited";
                                }
                            }
                            purchases.add(new Purchase().setPurchase(target_dc, post_title, purchase.getString("target_sn"), seller_name, purchase.getString("post_creator_sn"), purchase.getString("price"), purchase.getString("created_datetime").substring(0, 16)));
                            purchaseRecyclerAdapter.notifyItemInserted(get_start_no + 1 + i);
                        }
                        get_start_no = get_start_no + purchaseHistories.length();
                        Handler mHandler = new Handler();
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                purchaseRecyclerAdapter.removeFooter();
                                isEnd = true;
                                purchaseRecyclerAdapter.notifyItemRemoved(get_start_no + 2);
                            }
                        }, 500);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
            studiosHttpGetRequestTask.execute(url);
        }
    }

}
