package com.diocian.diocianappdev.fragment;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.diocian.diocianappdev.MainActivity;
import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.adapter.PlaylistAdapter;
import com.diocian.diocianappdev.listener.EndlessRecyclerViewScrollListener;
import com.diocian.diocianappdev.model.Playlist;
import com.diocian.diocianappdev.util.GlobalConstants;
import com.diocian.diocianappdev.util.HttpRequest;
import com.diocian.diocianappdev.util.ImageFetcher;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PlaylistsFragment extends Fragment {
    private RecyclerView recyclerViewPlayLists;
    private ArrayList<Playlist> playlists = new ArrayList<>();
    private ArrayList<Playlist> playlists2 = new ArrayList<>();
    private PlaylistAdapter playlistAdapter;
    private ImageFetcher imageFetcher;
    private StaggeredGridLayoutManager playlistLayoutManager;

    private int get_start_no = 0;
    private boolean loading = false;

    private boolean isEnd = false;

    ImageView imageViewCoverImage;
    TextView textViewTitle;
    TextView textViewArtistName;

    boolean autoStart = false;
    boolean isReset = false;
    String post_sn = "";

    int contentsSize = 0;
    boolean isRendered = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imageFetcher = new ImageFetcher(getActivity());
        setHasOptionsMenu(true);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        playlistLayoutManager.setSpanCount(1);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_playlists, container, false);

//        view.setFocusableInTouchMode(true);
//        view.requestFocus();
//        view.setOnKeyListener(new View.OnKeyListener() {
//            @Override
//            public boolean onKey(View v, int keyCode, KeyEvent event) {
//                if( keyCode == event.KEYCODE_BACK) {
//                    ((MainActivity) getActivity()).hidePlaylists();;
//                    return true;
//                }
//                return false;
//            }
//        });

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        String toolbar_title = "".toUpperCase();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_expand_more_white_24dp);
        toolbar.setTitle(toolbar_title);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).hidePlaylists();
            }
        });

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            autoStart = bundle.getBoolean("autoStart");
            isReset = bundle.getBoolean("isReset");
            post_sn = bundle.getString("post_sn");
        }

        imageViewCoverImage = (ImageView) view.findViewById(R.id.imageViewCoverImage);
        textViewTitle = (TextView) view.findViewById(R.id.textViewTitle);
        textViewArtistName = (TextView) view.findViewById(R.id.textViewArtistName);

        recyclerViewPlayLists = (RecyclerView) view.findViewById(R.id.recyclerViewPlayLists);
        playlistLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL) {
            @Override
            public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
                super.onLayoutChildren(recycler, state);
                if (state.getItemCount() == (contentsSize + 1) && !isRendered) {
                    ((MainActivity) getActivity()).setPlaylistActive(((MainActivity) getActivity()).getPostSnActivePlaylist(), isReset, autoStart);
                    isRendered = true;
                }
            }
        };
        recyclerViewPlayLists.setLayoutManager(playlistLayoutManager);
        playlistAdapter = new PlaylistAdapter(playlists, imageFetcher, getContext(), autoStart, isReset, post_sn);
        if (isEnd) {
            playlistAdapter.removeFooter();
        }
        recyclerViewPlayLists.setAdapter(playlistAdapter);
        recyclerViewPlayLists.addOnScrollListener(new EndlessRecyclerViewScrollListener(playlistLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                loadMorePosts();
            }
        });

        if (playlists.size() == 0) {
            loadMorePosts();
        }

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_playlist, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

//        if (id == R.id.action_search) {
//            return true;
//        } else if (id == R.id.action_notification) {
//            return true;
//        }//else if (id == android.R.id.home) {
//            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
//            fragmentManager.popBackStackImmediate();
//            ((MainActivity) getActivity()).hidePlaylists();
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    public void loadMorePosts() {
        if (!loading && !isEnd) {
            loading = true;
            isRendered = false;
            Map<String, String> parameterMap = new HashMap<>();
            int get_count = 1000;
            parameterMap.put("get_count", String.valueOf(get_count));
            parameterMap.put("get_start_no", String.valueOf(get_start_no));
            String url = GlobalConstants.DOMAIN + "/getPlaylists.do?";
            for (String key : parameterMap.keySet()) {
                url += key + "=" + parameterMap.get(key) + "&";
            }
            HttpRequest httpRequest = new HttpRequest(getContext());
            HttpRequest.HttpGetRequestTask playListsHttpGetRequestTask = httpRequest.newHttpGetRequestTask(new HttpRequest.CallbackIf() {
                @Override
                public void callback(JSONObject jsonObject, Context context) {
                    try {

                        JSONArray jsonArrayPlayLists = jsonObject.getJSONArray("playlists");
                        contentsSize = jsonArrayPlayLists.length();
                        JSONObject jsonObjectPlaylist;
                        boolean firstPlaylist = false;
                        for (int i = 0; i < jsonArrayPlayLists.length(); i++) {
                            jsonObjectPlaylist = jsonArrayPlayLists.getJSONObject(i);
                            playlists.add(new Playlist().setPlaylistFromJSONObject(jsonObjectPlaylist));
                            playlistAdapter.notifyItemInserted(get_start_no + 1 + i);
                        }
                        if (((MainActivity) getActivity()) != null) {
                            ((MainActivity) getActivity()).setPlaylistForService(playlists2);
                        }
                        get_start_no = get_start_no + jsonArrayPlayLists.length();
                        Handler mHandler = new Handler();
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                playlistAdapter.removeFooter();
                                isEnd = true;
                                playlistAdapter.notifyItemRemoved(get_start_no + 2);
                            }
                        }, 500);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
            playListsHttpGetRequestTask.execute(url);
        }
    }


}
