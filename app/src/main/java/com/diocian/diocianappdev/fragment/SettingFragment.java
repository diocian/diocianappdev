package com.diocian.diocianappdev.fragment;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.OpenableColumns;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.diocian.diocianappdev.MainActivity;
import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.util.GlobalConstants;
import com.diocian.diocianappdev.util.HttpRequestPost;
import com.diocian.diocianappdev.util.ImageFetcher;
import com.diocian.diocianappdev.util.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SettingFragment extends Fragment {

    View view;

    TextInputLayout textInputLayoutMemberName;
    AppCompatEditText editTextMemberName;

    TextInputLayout textInputLayoutMemberSummary;
    AppCompatEditText editTextMemberSummary;

    private ImageFetcher imageFetcher;
    ImageView ImageViewUserProfileImage;
    Uri profileFileUri;
    String profileFileBase64;

    ImageView imageViewCoverImage;
    Uri coverFileUri;
    String coverFileBase64;


    public SettingFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        imageFetcher = new ImageFetcher(getContext());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_setting, container, false);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.app_bar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        String toolbar_title = "Setting".toUpperCase();
        toolbar.setTitle(toolbar_title);

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        List<String> locations = new ArrayList<>();
        locations.add("United States");
        locations.add("日本");
        locations.add("한국");
        locations.add("Việt Nam");
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item, locations);
        AppCompatSpinner spinnerLocation = (AppCompatSpinner) view.findViewById(R.id.spinnerLocation);
        spinnerLocation.setAdapter(adapter);

        String location = ((MainActivity) getActivity()).getPreferences("location");
        int selectedPosition = 0;
        for (int i = 0; i < GlobalConstants.LOCATION_CODES.length; i++) {
            if(GlobalConstants.LOCATION_CODES[i].equals(location)) {
                selectedPosition = i;
            }
        }

        spinnerLocation.setSelection(selectedPosition);
        spinnerLocation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((MainActivity) getActivity()).savePreferences("location", GlobalConstants.LOCATION_CODES[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        textInputLayoutMemberName = (TextInputLayout) view.findViewById(R.id.textInputLayoutMemberName);
        editTextMemberName = (AppCompatEditText) view.findViewById(R.id.editTextMemberName);
        editTextMemberName.setText(((MainActivity) getActivity()).getPreferences("member_name"));

        textInputLayoutMemberSummary = (TextInputLayout) view.findViewById(R.id.textInputLayoutMemberSummary);
        editTextMemberSummary = (AppCompatEditText) view.findViewById(R.id.editTextMemberSummary);
        editTextMemberSummary.setText(((MainActivity) getActivity()).getPreferences("member_summary"));

        Utils.addTextChangedListenerToCheckLengthInTextInputLayout(textInputLayoutMemberName, 0, "Name is required.");
        Utils.addTextChangedListenerToCheckLengthInTextInputLayout(textInputLayoutMemberSummary, 0, "Summary is required.");

        ImageViewUserProfileImage = (ImageView) view.findViewById(R.id.ImageViewUserProfileImage);
        imageFetcher.setImageToImageView(GlobalConstants.DOMAIN + ((MainActivity) getActivity()).getPreferences("member_image_url"), ImageViewUserProfileImage, 64);

        ImageButton imageButtonProfileImageUpload = (ImageButton) view.findViewById(R.id.imageButtonProfileImageUpload);
        imageButtonProfileImageUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.checkPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE, 0)) {
                    showFileChooser("image/*", 0);
                }
            }
        });

        imageViewCoverImage = (ImageView) view.findViewById(R.id.imageViewCoverImage);
        imageFetcher.setImageToImageView(GlobalConstants.DOMAIN + ((MainActivity) getActivity()).getPreferences("home_image_url"), imageViewCoverImage, 320);

        ImageButton imageButtonCoverImageUpload = (ImageButton) view.findViewById(R.id.imageButtonCoverImageUpload);
        imageButtonCoverImageUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Utils.checkPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE, 0)) {
                    showFileChooser("image/*", 1);
                }
            }
        });

        Button buttonSave = (Button) view.findViewById(R.id.buttonSave);
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(((AppCompatActivity) getActivity()).INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                String member_name = editTextMemberName.getText().toString();
                String member_summary = editTextMemberSummary.getText().toString();

                Utils.checkLengthInTextInputLayout(textInputLayoutMemberName, 0, "Name is required.");
                Utils.checkLengthInTextInputLayout(textInputLayoutMemberSummary, 0, "Summary is required.");

                if (textInputLayoutMemberName.isErrorEnabled()) {
                } else if (textInputLayoutMemberSummary.isErrorEnabled()) {
                } else {
                    updateSetting(member_name, member_summary, coverFileBase64);
                }

            }
        });
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_none, menu);
        Utils.showMenuNotificationCount(menu, getActivity());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_notification) {
            ((MainActivity) getActivity()).viewNotifications();
            return true;
        } else if (id == android.R.id.home) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(((AppCompatActivity) getActivity()).INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            fragmentManager.popBackStackImmediate();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void updateSetting(String member_name, String member_summary, String coverFileBase64) {
        final String finalMember_name = member_name;
        final String finalMember_summary = member_summary;
        try {
            member_name = URLEncoder.encode(member_name, "UTF-8");
            member_summary = URLEncoder.encode(member_summary, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String url = GlobalConstants.DOMAIN + "/updateMember.do?member_name=" + member_name + "&member_summary=" + member_summary + "&member_sn=" + ((MainActivity) getActivity()).getPreferences("member_sn");

        if (profileFileBase64 != null) {
            url += "&imagesArray1[]=" + Utils.encodeUtf8(profileFileBase64);
        }

        if (coverFileBase64 != null) {
            url += "&imagesArray2[]=" + Utils.encodeUtf8(coverFileBase64);
        }

        HttpRequestPost httpRequest = new HttpRequestPost(getContext());
        HttpRequestPost.HttpGetRequestTask settingHttpGetRequestTask = httpRequest.newHttpGetRequestTask(new HttpRequestPost.CallbackIf() {
            @Override
            public void callback(JSONObject jsonObject, Context context) {
                try {
                    JSONObject result = jsonObject.getJSONObject("result");
                    if (result.getString("success_yn").equals("Y")) {
                        ((MainActivity) getActivity()).savePreferences("member_name", finalMember_name);
                        ((MainActivity) getActivity()).savePreferences("member_summary", finalMember_summary);
//                        ((MainActivity) getActivity()).savePreferences("member_image_url", member.getString("member_image_url"));
//                        ((MainActivity) getActivity()).savePreferences("home_image_url", member.getString("home_image_url"));
                        ((MainActivity) getActivity()).viewMember(((MainActivity) getActivity()).getPreferences("member_sn"));
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setMessage("Update failed.");
                        builder.setPositiveButton("Ok", null);
                        builder.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        settingHttpGetRequestTask.execute(url);

    }

    private void showFileChooser(String intentType, int fileTypeCode) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType(intentType);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        try {
            startActivityForResult(Intent.createChooser(intent, "Select a File to Upload"), fileTypeCode);
        } catch (android.content.ActivityNotFoundException e) {
        }
    }

    final static long MAXIMUM_FILE_SIZE = 104857600;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 0:
                if (resultCode == FragmentActivity.RESULT_OK) {
                    profileFileUri = data.getData();

                    ContentResolver contentResolver = getContext().getContentResolver();
                    Cursor cursor = contentResolver.query(profileFileUri, null, null, null, null);
                    if (cursor != null) {
                        int nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                        cursor.moveToFirst();
                        String fileName = cursor.getString(nameIndex);
                        nameIndex = cursor.getColumnIndex(OpenableColumns.SIZE);
                        cursor.moveToFirst();
                        long fileSize = Long.parseLong(cursor.getString(nameIndex));
                        cursor.close();

                        if (fileSize > MAXIMUM_FILE_SIZE) {
                            profileFileUri = null;
                        } else {
                            Utils.rotateAndResizeImageFromUri(getActivity(), profileFileUri, ImageViewUserProfileImage, 64, 64);
                            profileFileBase64 = Utils.encodeBase64FromUri(getActivity(), Utils.rotateAndResizeImageFromUri(getActivity(), profileFileUri, ImageViewUserProfileImage, 64, 64));
                        }
                    }
                }
                break;
            case 1:
                if (resultCode == FragmentActivity.RESULT_OK) {
                    coverFileUri = data.getData();

                    ContentResolver contentResolver = getContext().getContentResolver();
                    Cursor cursor = contentResolver.query(coverFileUri, null, null, null, null);

                    if (cursor != null) {
                        int nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                        cursor.moveToFirst();
                        String fileName = cursor.getString(nameIndex);
                        nameIndex = cursor.getColumnIndex(OpenableColumns.SIZE);
                        cursor.moveToFirst();
                        long fileSize = Long.parseLong(cursor.getString(nameIndex));
                        cursor.close();

                        if (fileSize > MAXIMUM_FILE_SIZE) {
                            profileFileUri = null;
                        } else {
                            Utils.rotateAndResizeImageFromUri(getActivity(), coverFileUri, imageViewCoverImage, 328, 246);
                            coverFileBase64 = Utils.encodeBase64FromUri(getActivity(), Utils.rotateAndResizeImageFromUri(getActivity(), coverFileUri, imageViewCoverImage, 328, 246));

                        }
                    }
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private static int exifToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270;
        }
        return 0;
    }


}