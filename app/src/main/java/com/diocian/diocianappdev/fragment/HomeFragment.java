package com.diocian.diocianappdev.fragment;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.diocian.diocianappdev.MainActivity;
import com.diocian.diocianappdev.model.Post;
import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.adapter.PortletMarketRecyclerAdapter;
import com.diocian.diocianappdev.adapter.PortletMusicRecyclerAdapter;
import com.diocian.diocianappdev.adapter.PortletStudioRecyclerAdapter;
import com.diocian.diocianappdev.adapter.PortletStudynoteRecyclerAdapter;
import com.diocian.diocianappdev.adapter.PortletVideoRecyclerAdapter;
import com.diocian.diocianappdev.util.GlobalConstants;
import com.diocian.diocianappdev.util.HttpRequest;
import com.diocian.diocianappdev.util.ImageFetcher;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import com.diocian.diocianappdev.util.HttpRequest.*;
import com.diocian.diocianappdev.util.Utils;

public class HomeFragment extends Fragment {

    private ImageFetcher imageFetcher;
    private RecyclerView portletStudynoteRecyclerView;
    private RecyclerView portletStudioRecyclerView;
    private RecyclerView portletMusicRecyclerView;
    private RecyclerView portletVideoRecyclerView;
    private RecyclerView portletMarketRecyclerView;
    private ArrayList<Post> studynotes;
    private ArrayList<Post> studios;
    private ArrayList<Post> musics;
    private ArrayList<Post> videos;
    private ArrayList<Post> markets;
    private PortletStudynoteRecyclerAdapter portletStudynoteRecyclerAdapter;
    private PortletStudioRecyclerAdapter portletStudioRecyclerAdapter;
    private PortletMusicRecyclerAdapter portletMusicRecyclerAdapter;
    private PortletVideoRecyclerAdapter portletVideoRecyclerAdapter;
    private PortletMarketRecyclerAdapter portletMarketRecyclerAdapter;
    View view;

    public HomeFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        imageFetcher = new ImageFetcher(getActivity());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        portletStudynoteRecyclerView.removeAllViews();
//        portletStudioRecyclerView.removeAllViews();
//        portletMusicRecyclerView.removeAllViews();
//        portletVideoRecyclerView.removeAllViews();
//        portletMarketRecyclerView.removeAllViews();
//        portletStudynoteRecyclerView = null;
//        portletStudioRecyclerView = null;
//        portletMusicRecyclerView = null;
//        portletVideoRecyclerView = null;
//        portletMarketRecyclerView = null;
//        System.gc();
    }

    String search_word;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_home, container, false);

        Bundle bundle = this.getArguments();
        boolean isNotification = false;
        search_word = null;
        if (bundle != null) {
            isNotification = bundle.getBoolean("isNotification");
            bundle.putBoolean("isNotification", false);
            search_word = bundle.getString("query");
        }
        if (search_word == null) {
            search_word = "";
        }
        if (isNotification) {
            Utils.replaceFragment((MainActivity) getActivity(), new NotificationsFragment(), new Bundle());
        }

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.app_bar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(getActivity(), drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
        };
        if (drawer != null) {
            drawer.addDrawerListener(toggle);
        }
        toggle.syncState();

        ImageView bannerImageView = (ImageView) view.findViewById(R.id.bannerImageView);
        bannerImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                Uri u = Uri.parse("https://www.youtube.com/watch?v=PLY8BGNC4s0");
                i.setData(u);
                getActivity().startActivity(i);
            }
        });


        // TODO : B 오디션 banner

        portletMusicRecyclerView = (RecyclerView) view.findViewById(R.id.portlet_music_recyclerView);
        musics = new ArrayList<>();
        portletMusicRecyclerAdapter = new PortletMusicRecyclerAdapter(musics, imageFetcher, getContext());
        portletMusicRecyclerView.setAdapter(portletMusicRecyclerAdapter);

        HttpRequest httpRequest = new HttpRequest(getContext());
        HttpRequest.HttpGetRequestTask musicsHttpGetRequestTask = httpRequest.newHttpGetRequestTask(new CallbackIf() {
            @Override
            public void callback(JSONObject jsonObject, Context context) {
                try {
                    JSONArray posts = jsonObject.getJSONArray("posts");
                    if (posts.length() > 0) {
                        view.findViewById(R.id.portlet_musics).setVisibility(View.VISIBLE);
                    }
                    JSONArray images = new JSONArray();
                    if (jsonObject.has("images")) {
                        images = jsonObject.getJSONArray("images");
                    }
                    JSONObject post, image;
                    String imageUrl;
                    for (int i = 0; i < posts.length(); i++) {
                        post = posts.getJSONObject(i);
                        imageUrl = null;
                        for (int j = 0; j < images.length(); j++) {
                            image = images.getJSONObject(j);
                            if (post.getString("post_sn").equals(image.getString("target_sn"))) {
                                imageUrl = GlobalConstants.DOMAIN + image.getString("file_url");
                                break;
                            }
                        }
                        musics.add(new Post().setMusic(post.getString("post_sn"), post.getString("genre_name"), imageUrl, post.getString("title"), post.getString("artist_name"), GlobalConstants.DOMAIN + post.getString("audio_file_url")));
                    }
                    portletMusicRecyclerAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        musicsHttpGetRequestTask.execute(GlobalConstants.DOMAIN + "/getPosts.do?target_dc=music&get_start_no=0&get_count=10&search_word=" + search_word);

        portletVideoRecyclerView = (RecyclerView) view.findViewById(R.id.portlet_video_recyclerView);
        videos = new ArrayList<>();
        portletVideoRecyclerAdapter = new PortletVideoRecyclerAdapter(videos, imageFetcher, getContext());
        portletVideoRecyclerView.setAdapter(portletVideoRecyclerAdapter);

        HttpRequest.HttpGetRequestTask videosHttpGetRequestTask = httpRequest.newHttpGetRequestTask(new CallbackIf() {
            @Override
            public void callback(JSONObject jsonObject, Context context) {
                try {
                    JSONArray posts = jsonObject.getJSONArray("posts");
                    if (posts.length() > 0) {
                        view.findViewById(R.id.portlet_videos).setVisibility(View.VISIBLE);
                    }
                    JSONObject post;
                    for (int i = 0; i < posts.length(); i++) {
                        post = posts.getJSONObject(i);
                        videos.add(new Post().setVideo(post.getString("youtube_video_id"), post.getString("post_sn"), post.getString("genre_name"), "http://img.youtube.com/vi/" + post.getString("youtube_video_id") + "/0.jpg", post.getString("title"), post.getString("member_name")));
                    }
                    portletVideoRecyclerAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        videosHttpGetRequestTask.execute(GlobalConstants.DOMAIN + "/getPosts.do?target_dc=video&get_start_no=0&get_count=10&search_word=" + search_word);

        portletStudynoteRecyclerView = (RecyclerView) view.findViewById(R.id.portlet_studynote_recyclerView);
        studynotes = new ArrayList<>();
        portletStudynoteRecyclerAdapter = new PortletStudynoteRecyclerAdapter(studynotes, imageFetcher, getContext());
        portletStudynoteRecyclerView.setAdapter(portletStudynoteRecyclerAdapter);

        HttpRequest.HttpGetRequestTask studynotesHttpGetRequestTask = httpRequest.newHttpGetRequestTask(new CallbackIf() {
            @Override
            public void callback(JSONObject jsonObject, Context context) {
//                Log.d("TEST----->", "portletStudynoteRecyclerAdapter : " + portletStudynoteRecyclerView.getAdapter());
                try {
                    JSONArray posts = jsonObject.getJSONArray("posts");
                    if (posts.length() > 0) {
                        view.findViewById(R.id.portlet_samples).setVisibility(View.VISIBLE);
                    }
                    JSONArray images = new JSONArray();
                    if (jsonObject.has("images")) {
                        images = jsonObject.getJSONArray("images");
                    }
                    JSONObject post, image;
                    String imageUrl;
                    int duration_min;
                    int duration_sec;
                    for (int i = 0; i < posts.length(); i++) {
                        post = posts.getJSONObject(i);
                        duration_min = Math.round(Float.parseFloat(post.getString("play_sec")) / 60);
                        duration_sec = Math.round(Float.parseFloat(post.getString("play_sec")) % 60);
                        imageUrl = null;
                        for (int j = 0; j < images.length(); j++) {
                            image = images.getJSONObject(j);
                            if (post.getString("post_sn").equals(image.getString("target_sn"))) {
                                imageUrl = GlobalConstants.DOMAIN + image.getString("file_url");
                                break;
                            }
                        }
                        studynotes.add(new Post().setStudynote(post.getString("post_sn"), imageUrl, post.getString("genre_name"), post.getString("job_name"), "$" + post.getString("price"), duration_min + ":" + duration_sec, GlobalConstants.DOMAIN + post.getString("image_file_url"), post.getString("title"), post.getString("member_name"), GlobalConstants.DOMAIN + post.getString("audio_file_url")));
                    }
                    portletStudynoteRecyclerAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        studynotesHttpGetRequestTask.execute(GlobalConstants.DOMAIN + "/getPosts.do?target_dc=study%20note&get_start_no=0&get_count=10&search_word=" + search_word);

        portletStudioRecyclerView = (RecyclerView) view.findViewById(R.id.portlet_studio_recyclerView);
        studios = new ArrayList<>();
        portletStudioRecyclerAdapter = new PortletStudioRecyclerAdapter(studios, getContext());
        portletStudioRecyclerView.setAdapter(portletStudioRecyclerAdapter);

        HttpRequest.HttpGetRequestTask studiosHttpGetRequestTask = httpRequest.newHttpGetRequestTask(new CallbackIf() {
            @Override
            public void callback(JSONObject jsonObject, Context context) {
                try {
                    JSONArray posts = jsonObject.getJSONArray("posts");
                    if (posts.length() > 0) {
                        view.findViewById(R.id.portlet_studios).setVisibility(View.VISIBLE);
                    }
                    JSONObject post;
                    for (int i = 0; i < posts.length(); i++) {
                        post = posts.getJSONObject(i);
                        studios.add(new Post().setStudio(post.getString("post_sn"), post.getString("genre_name"), post.getString("job_name").replaceAll(",", " "), post.getString("title"), post.getString("member_name")));
                    }
                    portletStudioRecyclerAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        studiosHttpGetRequestTask.execute(GlobalConstants.DOMAIN + "/getPosts.do?target_dc=studio&get_start_no=0&get_count=10&search_word=" + search_word);

        portletMarketRecyclerView = (RecyclerView) view.findViewById(R.id.portlet_market_recyclerView);
        markets = new ArrayList<>();
        portletMarketRecyclerAdapter = new PortletMarketRecyclerAdapter(markets, imageFetcher, getContext());
        portletMarketRecyclerView.setAdapter(portletMarketRecyclerAdapter);

        HttpRequest.HttpGetRequestTask marketsHttpGetRequestTask = httpRequest.newHttpGetRequestTask(new CallbackIf() {
            @Override
            public void callback(JSONObject jsonObject, Context context) {
                try {
                    JSONArray posts = jsonObject.getJSONArray("posts");
                    if (posts.length() > 0) {
                        view.findViewById(R.id.portlet_markets).setVisibility(View.VISIBLE);
                    }
                    JSONArray images = new JSONArray();
                    if (jsonObject.has("images")) {
                        images = jsonObject.getJSONArray("images");
                    }
                    JSONObject post, image;
                    String imageUrl;
                    for (int i = 0; i < posts.length(); i++) {
                        post = posts.getJSONObject(i);
                        imageUrl = null;
                        for (int j = 0; j < images.length(); j++) {
                            image = images.getJSONObject(j);
                            if (post.getString("post_sn").equals(image.getString("target_sn"))) {
                                imageUrl = GlobalConstants.DOMAIN + image.getString("file_url");
                                break;
                            }
                        }
                        String price = "";
                        String location = ((MainActivity) getActivity()) != null ? ((MainActivity) getActivity()).getPreferences("location") : "US";
                        if (location.equals("US")) {
                            price = "＄" + post.getString("US_price");
                        } else if (location.equals("JP")) {
                            price = "￥" + post.getString("JP_price");
                        } else if (location.equals("KR")) {
                            price = "￦" + post.getString("KR_price");
                        } else if (location.equals("VN")) {
                            price = "＄" + post.getString("VN_price");
                        }
                        markets.add(new Post().setMarket(post.getString("post_sn"), price, imageUrl, post.getString("title"), post.getString("member_name")));

                    }
                    portletMarketRecyclerAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        marketsHttpGetRequestTask.execute(GlobalConstants.DOMAIN + "/getPosts.do?target_dc=market&get_start_no=0&get_count=10&search_word=" + search_word);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_home, menu);
        Utils.showMenuNotificationCount(menu, getActivity());

        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        final SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setQuery(search_word, false);
        LinearLayout linearLayout1 = (LinearLayout) searchView.getChildAt(0);
        LinearLayout linearLayout2 = (LinearLayout) linearLayout1.getChildAt(2);
        LinearLayout linearLayout3 = (LinearLayout) linearLayout2.getChildAt(1);
        AutoCompleteTextView autoComplete = (AutoCompleteTextView) linearLayout3.getChildAt(0);
        int px = (int) (24 * this.getResources().getDisplayMetrics().density);
        autoComplete.setDropDownVerticalOffset(px);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_search) {
            return true;
        } else if (id == R.id.action_notification) {
            ((MainActivity) getActivity()).viewNotifications();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
