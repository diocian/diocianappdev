package com.diocian.diocianappdev.fragment;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.adapter.MrsRecyclerAdapter;
import com.diocian.diocianappdev.listener.EndlessRecyclerViewScrollListener;
import com.diocian.diocianappdev.model.Post;
import com.diocian.diocianappdev.util.GlobalConstants;
import com.diocian.diocianappdev.util.HttpRequest;
import com.diocian.diocianappdev.util.ImageFetcher;
import com.diocian.diocianappdev.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Bmv1HomeFragment extends Fragment {
    private RecyclerView videoRecyclerView;
    private ArrayList<Post> videos = new ArrayList<>();
    private MrsRecyclerAdapter mrsRecyclerAdapter;
    private ImageFetcher imageFetcher;
    private StaggeredGridLayoutManager videoLayoutManager;

    private int get_start_no = 0;
    private boolean loading = false;

    private boolean isEnd = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imageFetcher = new ImageFetcher(getActivity());
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        videoLayoutManager.setSpanCount(Utils.calColumnCount(getContext()));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        videoRecyclerView.removeAllViews();
//        videoRecyclerView = null;
//        System.gc();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_videos, container, false);

        videoRecyclerView = (RecyclerView) view.findViewById(R.id.videos_recyclerView);
        videoLayoutManager = new StaggeredGridLayoutManager(Utils.calColumnCount(getContext()), StaggeredGridLayoutManager.VERTICAL);
        videoRecyclerView.setLayoutManager(videoLayoutManager);
        mrsRecyclerAdapter = new MrsRecyclerAdapter(videos, imageFetcher, getContext());
        if (isEnd) {
            mrsRecyclerAdapter.removeFooter();
        }
        videoRecyclerView.setAdapter(mrsRecyclerAdapter);
        videoRecyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(videoLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                loadMorePosts();
            }
        });

        if (videos.size() == 0) {
            loadMorePosts();
        }

        return view;
    }

    public void loadMorePosts() {
        if (!loading && !isEnd) {
            loading = true;
            Map<String, String> parameterMap = new HashMap<>();
//            String target_dc = "video";
//            parameterMap.put("target_dc", target_dc);
//            parameterMap.put("get_start_no", String.valueOf(get_start_no));
//            int get_count = 12;
//            parameterMap.put("get_count", String.valueOf(get_count));
//            String url = GlobalConstants.DOMAIN + "/getPosts.do?";
//            for (String key : parameterMap.keySet()) {
//                url += key + "=" + parameterMap.get(key) + "&";
//            }
            String url = GlobalConstants.DOMAIN + "/json/bmv1.jsp";
            HttpRequest httpRequest = new HttpRequest(getContext());
            HttpRequest.HttpGetRequestTask studiosHttpGetRequestTask = httpRequest.newHttpGetRequestTask(new HttpRequest.CallbackIf() {
                @Override
                public void callback(JSONObject jsonObject, Context context) {
                    try {
                        String lang_cd = getResources().getString(R.string.lang_cd);
                        JSONArray mrs = jsonObject.getJSONArray("mrs");
                        JSONObject mr;
                        for (int i = 0; i < mrs.length(); i++) {
                            mr = mrs.getJSONObject(i);

                            videos.add(new Post().setMr(mr.getString("post_sn"), mr.getJSONObject("title").getString(lang_cd), mr.getJSONObject("artist_name").getString(lang_cd), GlobalConstants.DOMAIN + mr.getString("cover_image_url"), mr.getString("audio_file_url"), mr.getString("youtube_video_id")));
                            mrsRecyclerAdapter.notifyItemInserted(get_start_no + 1 + i);
                        }
                        get_start_no = get_start_no + mrs.length();
                        Handler mHandler = new Handler();
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mrsRecyclerAdapter.removeFooter();
                                isEnd = true;
                                mrsRecyclerAdapter.notifyItemRemoved(get_start_no + 2);
                            }
                        }, 500);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
            studiosHttpGetRequestTask.execute(url);
        }
    }

}
