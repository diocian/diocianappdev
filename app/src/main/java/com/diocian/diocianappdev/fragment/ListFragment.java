package com.diocian.diocianappdev.fragment;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design .widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;

import com.diocian.diocianappdev.MainActivity;
import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.adapter.ListPagerAdapter;
import com.diocian.diocianappdev.util.GlobalConstants;
import com.diocian.diocianappdev.util.Utils;

public class ListFragment extends Fragment {

    private int[] tabIcons = {
            R.drawable.ic_music_24dp,
            R.drawable.ic_video_24dp,
            R.drawable.ic_studynote_24dp,
            R.drawable.ic_studio_24dp,
            R.drawable.ic_market_24dp
    };

    private int[] tabTitles = {
            R.string.music,
            R.string.video,
            R.string.sample,
            R.string.studio,
            R.string.market
    };

    private static TabLayout tabLayout;
    ViewPager mViewPager;

    private  String search_word;

    public ListFragment() {
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        System.gc();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_list, container, false);

        search_word = null;
        MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity != null) {
            search_word = mainActivity.query;
        }
        if (search_word == null) {
            search_word = "";
        }

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.app_bar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        toolbar.setTitle(getResources().getString(tabTitles[0]).toUpperCase());

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        ListPagerAdapter mSectionsPagerAdapter = new ListPagerAdapter(getChildFragmentManager());
        mViewPager = (ViewPager) view.findViewById(R.id.container);
        mSectionsPagerAdapter.addFragment(new MusicsFragment(), getResources().getString(R.string.music));
        mSectionsPagerAdapter.addFragment(new VideosFragment(), getResources().getString(R.string.video));
        mSectionsPagerAdapter.addFragment(new StudynotesFragment(), getResources().getString(R.string.sample));
        mSectionsPagerAdapter.addFragment(new StudiosFragment(), getResources().getString(R.string.studio));
        mSectionsPagerAdapter.addFragment(new MarketsFragment(), getResources().getString(R.string.market));

        mViewPager.setAdapter(mSectionsPagerAdapter);

        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            tabLayout.getTabAt(i).setIcon(tabIcons[i]);
        }

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager) {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                super.onTabSelected(tab);
                ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
                if (actionBar != null) {
                    actionBar.setTitle(getResources().getString(tabTitles[tab.getPosition()]).toUpperCase());
                }
            }
        });

        Bundle bundle = this.getArguments();
        int tabNo = 0;
        if (bundle != null) {
            tabNo = bundle.getInt("tabNo");
        }
        setSelectedTab(tabNo);

        return view;
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_list, menu);
        Utils.showMenuNotificationCount(menu, getActivity());

        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setQuery(search_word, false);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_notification) {
            ((MainActivity) getActivity()).viewNotifications();
            return true;
        } else if (id == android.R.id.home) {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            fragmentManager.popBackStackImmediate();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void setSelectedTab(int tabNo) {
        TabLayout.Tab selectedTab = tabLayout.getTabAt(tabNo);
        if (selectedTab != null) {
            selectedTab.select();
        }
    }


}
