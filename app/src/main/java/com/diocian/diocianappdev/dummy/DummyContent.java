package com.diocian.diocianappdev.dummy;

import android.graphics.drawable.Drawable;

import com.diocian.diocianappdev.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DummyContent {

    public static final List<DummyItem> ITEMS = new ArrayList<DummyItem>();

    public static final Map<String, DummyItem> ITEM_MAP = new HashMap<String, DummyItem>();

    static {
//        addItem(createDummyItem(1,"Test01",R.drawable.music01));
//        addItem(createDummyItem(2,"Test02",R.drawable.music02));
//        addItem(createDummyItem(3,"Test03",R.drawable.music03));
//        addItem(createDummyItem(4,"Test04",R.drawable.music04));
//        addItem(createDummyItem(5,"Test05",R.drawable.music05));
//        addItem(createDummyItem(6,"Test06",R.drawable.music06));
//        addItem(createDummyItem(7,"Test07",R.drawable.music07));
//        addItem(createDummyItem(8,"Test08",R.drawable.music08));
    }

    private static void addItem(DummyItem item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);
    }

    private static DummyItem createDummyItem(int position, String text, int image) {
        return new DummyItem(String.valueOf(position), "Item " + position, makeDetails(position), text, image);
    }

    private static String makeDetails(int position) {
        StringBuilder builder = new StringBuilder();
        builder.append("Details about Item: ").append(position);
        for (int i = 0; i < position; i++) {
            builder.append("\nMore details information here.");
        }
        return builder.toString();
    }

    public static class DummyItem {
        public final String id;
        public final String content;
        public final String details;
        public final String text;
        public final int image;

        public DummyItem(String id, String content, String details, String text, int image) {
            this.id = id;
            this.content = content;
            this.details = details;
            this.text = text;
            this.image = image;
        }

        @Override
        public String toString() {
            return content;
        }
    }
}
