package com.diocian.diocianappdev.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.TextView;

import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.util.Typefaces;

public class TextViewCustom extends TextView {

    public TextViewCustom(Context context) {
        super(context);
        init(context, null, 0);
    }

    public TextViewCustom(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public TextViewCustom(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs, defStyle);
    }

    private void init(Context context, AttributeSet attrs, int defStyle) {
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.TextViewFont, 0, 0);
        try {
            String fontInAssets = ta.getString(R.styleable.TextViewFont_fontFile);
            setTypeface(Typefaces.get(context, fontInAssets));
        } finally {
            ta.recycle();
        }
    }

}
