package com.diocian.diocianappdev.adapter;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.diocian.diocianappdev.MainActivity;
import com.diocian.diocianappdev.model.Post;
import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.util.ImageFetcher;

import java.io.IOException;
import java.util.ArrayList;

public class PortletMusicRecyclerAdapter extends RecyclerView.Adapter<PortletMusicRecyclerAdapter.ViewHolder> {

    private ArrayList<Post> musics;
    private ImageFetcher imageFetcher;
    private Context context;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public View mView;
        public TextView genre_name;
        public ImageView image;
        public TextView title;
        public TextView artist_name;
        public String audio_file_url;
        public String post_sn;
        public TextView post_snTextView;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            genre_name = (TextView) view.findViewById(R.id.genre_name);
            image = (ImageView) view.findViewById(R.id.image);
            title = (TextView) view.findViewById(R.id.title);
            artist_name = (TextView) view.findViewById(R.id.artist_name);
            post_snTextView = (TextView) view.findViewById(R.id.post_sn);
        }
    }

    public PortletMusicRecyclerAdapter(ArrayList<Post> posts_param, ImageFetcher imageFetcher, Context context) {
        musics = posts_param;
        this.imageFetcher = imageFetcher;
        this.context = context;
    }

    @Override
    public PortletMusicRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.portlet_item_music, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.genre_name.setText(musics.get(position).getGenre_name());
        holder.title.setText(musics.get(position).getTitle());
        holder.artist_name.setText(musics.get(position).getArtist_name());
        imageFetcher.setImageToImageView(musics.get(position).getImage_file_url(), holder.image, 104);
        holder.audio_file_url = musics.get(position).getAudio_file_url();
        holder.post_sn = musics.get(position).getPost_sn();
        holder.post_snTextView.setText(holder.post_sn);

        // TODO : Music portlet link UI(or interaction 표준화)

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                ((MainActivity) context).viewMusic(holder.post_sn);
                ((MainActivity) context).playMediaPlayer(musics.get(position));
            }
        });

    }

    @Override
    public int getItemCount() {
        return musics.size();
    }

}
