package com.diocian.diocianappdev.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.diocian.diocianappdev.MainActivity;
import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.model.Post;
import com.diocian.diocianappdev.util.GlobalConstants;
import com.diocian.diocianappdev.util.ImageFetcher;

import java.util.ArrayList;

public class ContentRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Post> studynotes;
    private ImageFetcher imageFetcher;
    private Context context;

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private static final int TYPE_FOOTER = 2;

    private boolean isFooter = true;

    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        public HeaderViewHolder(View view) {
            super(view);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView genre_name;
        public TextView job_name;
        public ImageView image;
        public TextView title;
        public TextView created_datetime;
        public TextView like_count;
        public TextView comment_count;
        public TextView creator_name;
        public ImageView creator_image;
        public TextView price;
        public TextView duration;
        public String post_sn;

        public ViewHolder(View view) {
            super(view);
            genre_name = (TextView) view.findViewById(R.id.genre_name);
            job_name = (TextView) view.findViewById(R.id.job_name);
            image = (ImageView) view.findViewById(R.id.image);
            title = (TextView) view.findViewById(R.id.title);
            created_datetime = (TextView) view.findViewById(R.id.created_datetime);
            like_count = (TextView) view.findViewById(R.id.like_count);
            comment_count = (TextView) view.findViewById(R.id.comment_count);
            creator_name = (TextView) view.findViewById(R.id.creator_name);
            creator_image = (ImageView) view.findViewById(R.id.creator_image);
            price = (TextView) view.findViewById(R.id.price);
            duration = (TextView) view.findViewById(R.id.duration);
        }
    }

    public class FooterViewHolder extends RecyclerView.ViewHolder {
        public FooterViewHolder(View view) {
            super(view);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (isFooter) {
            if (position == 0) {
                return TYPE_HEADER;
            } else if (position == (studynotes.size() + 1)) {
                return TYPE_FOOTER;
            }
        } else {
            if (position == 0) {
                return TYPE_HEADER;
            }
        }
        return TYPE_ITEM;
    }

    public ContentRecyclerAdapter(ArrayList<Post> posts_param, ImageFetcher imageFetcher, Context context) {
        studynotes = posts_param;
        this.imageFetcher = imageFetcher;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_header, parent, false);
            return new HeaderViewHolder(view);
        } else if (viewType == TYPE_FOOTER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_footer, parent, false);
            return new FooterViewHolder(view);
        }
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_content, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            final ViewHolder itemHolder = (ViewHolder) holder;
            Post studynote = studynotes.get(position - 1);
            if (!studynote.getJob_name().equals("")) {
                itemHolder.genre_name.setVisibility(View.GONE);
                itemHolder.job_name.setVisibility(View.VISIBLE);
            }else{
                itemHolder.genre_name.setVisibility(View.VISIBLE);
                itemHolder.job_name.setVisibility(View.GONE);
            }
            itemHolder.genre_name.setText(studynote.getGenre_name());
            itemHolder.job_name.setText(studynote.getJob_name());
            imageFetcher.setImageToImageView(studynote.getWave_image_file_url(), itemHolder.image, 168);
            itemHolder.title.setText(studynote.getTitle());
            itemHolder.created_datetime.setText(studynote.getCreated_datetime());
            itemHolder.like_count.setText(studynote.getLike_count());
            itemHolder.comment_count.setText(studynote.getComment_count());
            itemHolder.creator_name.setText(studynote.getCreator_name());
            if (studynote.getCreator_image_file_url().equals(GlobalConstants.DOMAIN)) {
                itemHolder.creator_image.setImageResource(R.drawable.image_none2);
            }else{
                imageFetcher.setImageToImageView(studynote.getCreator_image_file_url(), itemHolder.creator_image, 24);
            }
            itemHolder.price.setText(studynote.getPrice());
            itemHolder.duration.setText(studynote.getDuration());
            itemHolder.post_sn = studynote.getPost_sn();
            itemHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MainActivity) context).viewStudynote(itemHolder.post_sn);
                }
            });

            // TODO : follow 확인/추가/삭제 기능.

        } else if (holder instanceof HeaderViewHolder) {
            StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
            layoutParams.setFullSpan(true);
        } else if (holder instanceof FooterViewHolder) {
            StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
            layoutParams.setFullSpan(true);
        }
    }

    @Override
    public int getItemCount() {
        if (isFooter) {
            return studynotes.size() + 2;
        }
        return studynotes.size() + 1;
    }

    public void removeFooter() {
        isFooter = false;
    }

}
