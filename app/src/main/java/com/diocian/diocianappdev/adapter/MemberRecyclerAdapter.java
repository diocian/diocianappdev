package com.diocian.diocianappdev.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.diocian.diocianappdev.MainActivity;
import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.fragment.ListFragment;
import com.diocian.diocianappdev.model.Member;
import com.diocian.diocianappdev.model.Post;
import com.diocian.diocianappdev.util.GlobalConstants;
import com.diocian.diocianappdev.util.ImageFetcher;

import java.util.ArrayList;

public class MemberRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Member> members;
    private ImageFetcher imageFetcher;
    private Context context;
    private Fragment fragment;
    private Bundle bundle;

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private static final int TYPE_FOOTER = 2;

    private boolean isFooter = true;

    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        public HeaderViewHolder(View view) {
            super(view);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewMemberSummary;
        public ImageView imageViewMemberImage;
        public TextView textViewMemberName;
        public ImageButton imageButtonFollow;
        public String member_sn;
        public String creator_sn;
        public String follow_sn;

        public ViewHolder(View view) {
            super(view);
            textViewMemberSummary = (TextView) view.findViewById(R.id.textViewMemberSummary);
            imageViewMemberImage = (ImageView) view.findViewById(R.id.imageViewMemberImage);
            textViewMemberName = (TextView) view.findViewById(R.id.textViewMemberName);
            imageButtonFollow = (ImageButton) view.findViewById(R.id.imageButtonFollow);
        }
    }

    public class FooterViewHolder extends RecyclerView.ViewHolder {
        public FooterViewHolder(View view) {
            super(view);
        }
    }

    public MemberRecyclerAdapter(ArrayList<Member> members_param, ImageFetcher imageFetcher, Context context, Fragment fragment, Bundle bundle) {
        members = members_param;
        this.imageFetcher = imageFetcher;
        this.context = context;
        this.fragment = fragment;
        this.bundle = bundle;
    }

    @Override
    public int getItemViewType(int position) {
        if (isFooter) {
            if (position == 0) {
                return TYPE_HEADER;
            } else if (position == (members.size() + 1)) {
                return TYPE_FOOTER;
            }
        } else {
            if (position == 0) {
                return TYPE_HEADER;
            }
        }
        return TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_header_none, parent, false);
            return new HeaderViewHolder(view);
        } else if (viewType == TYPE_FOOTER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_footer, parent, false);
            return new FooterViewHolder(view);
        }
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_member, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            final ViewHolder itemHolder = (ViewHolder) holder;
            Member member = members.get(position - 1);

            itemHolder.textViewMemberSummary.setText(member.getMember_summary());
            if (member.getMember_image_url().equals(GlobalConstants.DOMAIN)) {
                itemHolder.imageViewMemberImage.setImageResource(R.drawable.image_none2);
            } else {
                imageFetcher.setImageToImageView(member.getMember_image_url(), itemHolder.imageViewMemberImage, 24);
            }
            itemHolder.textViewMemberName.setText(member.getMember_name());

            itemHolder.member_sn = member.getMember_sn();
            itemHolder.creator_sn = member.getCreator_sn();

            itemHolder.textViewMemberName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MainActivity) context).viewMember(itemHolder.creator_sn);
                }
            });
            itemHolder.imageViewMemberImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MainActivity) context).viewMember(itemHolder.creator_sn);
                }
            });

            itemHolder.follow_sn = member.getFollow_sn();
            if (itemHolder.follow_sn != null && !itemHolder.follow_sn.equals("")) {
                itemHolder.imageButtonFollow.setImageResource(R.drawable.ic_person_add_diocian_18dp);

            } else {
                itemHolder.imageButtonFollow.setImageResource(R.drawable.ic_person_add_18dp);
            }

            itemHolder.imageButtonFollow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int myFollowCount;
                    if (itemHolder.follow_sn != null && !itemHolder.follow_sn.equals("")) {
                        myFollowCount = 1;
                    } else {
                        myFollowCount = 0;
                    }
                    ((MainActivity) context).setFollow(itemHolder.creator_sn, myFollowCount, fragment, bundle);
                }
            });

        } else if (holder instanceof HeaderViewHolder) {
            StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
            layoutParams.setFullSpan(true);
        } else if (holder instanceof FooterViewHolder) {
            StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
            layoutParams.setFullSpan(true);
        }
    }

    @Override
    public int getItemCount() {
        if (isFooter) {
            return members.size() + 2;
        }
        return members.size() + 1;
    }

    public void removeFooter() {
        isFooter = false;
    }

}
