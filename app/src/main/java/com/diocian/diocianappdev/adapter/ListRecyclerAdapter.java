package com.diocian.diocianappdev.adapter;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.diocian.diocianappdev.model.Post;
import com.diocian.diocianappdev.R;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.ArrayList;

public class ListRecyclerAdapter extends RecyclerView.Adapter<ListRecyclerAdapter.ViewHolder> {

    private ArrayList<Post> posts;

    public class ViewHolder extends RecyclerView.ViewHolder {
//        public ImageView post_image;
//        public TextView post_title;

        public ViewHolder(View view) {
            super(view);
//            post_image = (ImageView) view.findViewById(R.id.post_image);
//            post_title = (TextView) view.findViewById(R.id.post_title);
        }
    }

    public ListRecyclerAdapter(ArrayList<Post> posts_param) {
        posts = posts_param;
    }

    @Override
    public ListRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_music, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
//        new getHttpImage(holder.post_image).execute(posts.get(position).getImage_file_url());
//        holder.post_title.setText(posts.get(position).getTitle());
    }

    @Override
    public int getItemCount() {
        return posts.size();
    }

    private class getHttpImage extends AsyncTask<String, Void, Bitmap> {

        private final WeakReference<ImageView> imageWeakReference;

        private getHttpImage(ImageView imageView) {
            imageWeakReference = new WeakReference<ImageView>(imageView);
        }

        @Override
        protected Bitmap doInBackground(String... urls) {
            URL url;
            Bitmap bitmap = null;
            try {
                url = new URL(urls[0]);
//                BitmapFactory.Options options = new BitmapFactory.Options();
//                options.inSampleSize = 24;
                bitmap = BitmapFactory.decodeStream(url.openStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            ImageView imageViewUserHome = imageWeakReference.get();
            if (imageViewUserHome != null) {
                imageViewUserHome.setImageBitmap(bitmap);
            }
        }
    }


}
