package com.diocian.diocianappdev.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.diocian.diocianappdev.MainActivity;
import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.model.Purchase;
import com.diocian.diocianappdev.util.ImageFetcher;

import java.util.ArrayList;

public class PurchaseRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Purchase> purchaseHistory;
    private ImageFetcher imageFetcher;
    private Context context;

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private static final int TYPE_FOOTER = 2;

    private boolean isFooter = true;

    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        public HeaderViewHolder(View view) {
            super(view);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewDivision;
        public TextView textViewName;
        public TextView textViewSeller;
        public TextView textViewPrice;
        public TextView textViewPurchasedDate;
        public String post_sn;
        public String seller_sn;
        public String price;

        public ViewHolder(View view) {
            super(view);
            textViewDivision = (TextView) view.findViewById(R.id.textViewDivision);
            textViewName = (TextView) view.findViewById(R.id.textViewName);
            textViewSeller = (TextView) view.findViewById(R.id.textViewSeller);
            textViewPrice = (TextView) view.findViewById(R.id.textViewPrice);
            textViewPurchasedDate = (TextView) view.findViewById(R.id.textViewPurchasedDate);
        }
    }

    public class FooterViewHolder extends RecyclerView.ViewHolder {
        public FooterViewHolder(View view) {
            super(view);
        }
    }

    public PurchaseRecyclerAdapter(ArrayList<Purchase> posts_param, ImageFetcher imageFetcher, Context context) {
        purchaseHistory = posts_param;
        this.imageFetcher = imageFetcher;
        this.context = context;
    }

    @Override
    public int getItemViewType(int position) {
        if (isFooter) {
            if (position == 0) {
                return TYPE_HEADER;
            } else if (position == (purchaseHistory.size() + 1)) {
                return TYPE_FOOTER;
            }
        } else {
            if (position == 0) {
                return TYPE_HEADER;
            }
        }
        return TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_header_none, parent, false);
            return new HeaderViewHolder(view);
        } else if (viewType == TYPE_FOOTER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_footer, parent, false);
            return new FooterViewHolder(view);
        }
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_purchase, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            final ViewHolder itemHolder = (ViewHolder) holder;
            Purchase purchase = purchaseHistory.get(position - 1);

            itemHolder.textViewDivision.setText(purchase.getTarget_dc());
            itemHolder.textViewName.setText(purchase.getPost_title());
            itemHolder.textViewSeller.setText(purchase.getSeller_name());
            itemHolder.textViewPrice.setText(purchase.getPrice());
            itemHolder.textViewPurchasedDate.setText(purchase.getPurchased_date());
            itemHolder.post_sn = purchase.getPost_sn();
            itemHolder.seller_sn = purchase.getSeller_sn();

            itemHolder.textViewName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemHolder.textViewDivision.getText().equals("MUSIC")) {
                        ((MainActivity) context).viewMusic(itemHolder.post_sn);
                    } else if (itemHolder.textViewDivision.getText().equals("MARKET")) {
                        ((MainActivity) context).viewMarket(itemHolder.post_sn);
                    }
                }
            });

            if (!itemHolder.textViewSeller.getText().equals("")) {
                itemHolder.textViewSeller.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((MainActivity) context).viewMember(itemHolder.seller_sn);
                    }
                });
            }

        } else if (holder instanceof HeaderViewHolder) {
            StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
            layoutParams.setFullSpan(true);
        } else if (holder instanceof FooterViewHolder) {
            StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
            layoutParams.setFullSpan(true);
        }
    }

    @Override
    public int getItemCount() {
        if (isFooter) {
            return purchaseHistory.size() + 2;
        }
        return purchaseHistory.size() + 1;
    }

    public void removeFooter() {
        isFooter = false;
    }

}
