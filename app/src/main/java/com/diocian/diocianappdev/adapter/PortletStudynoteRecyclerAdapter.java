package com.diocian.diocianappdev.adapter;

import android.content.Context;
import android.media.MediaPlayer;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.diocian.diocianappdev.MainActivity;
import com.diocian.diocianappdev.model.Post;
import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.util.ImageFetcher;

import java.util.ArrayList;

public class PortletStudynoteRecyclerAdapter extends RecyclerView.Adapter<PortletStudynoteRecyclerAdapter.ViewHolder> {

    private ArrayList<Post> studynotes;
    private ImageFetcher imageFetcher;
    private Context context;


    public class ViewHolder extends RecyclerView.ViewHolder {
        public View mView;
        public TextView genre_name;
        public TextView job_name;
        public TextView price;
        public TextView duration;
        public ImageView wave_image;
        public TextView title;
        public TextView creator_name;
        public String audio_file_url;
        public String post_sn;
        public TextView post_snTextView;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            genre_name = (TextView) view.findViewById(R.id.genre_name);
            job_name = (TextView) view.findViewById(R.id.job_name);
            price = (TextView) view.findViewById(R.id.price);
            duration = (TextView) view.findViewById(R.id.duration);
            wave_image = (ImageView) view.findViewById(R.id.wave_image);
            title = (TextView) view.findViewById(R.id.title);
            creator_name = (TextView) view.findViewById(R.id.creator_name);
            post_snTextView = (TextView) view.findViewById(R.id.post_sn);

        }
    }

    public PortletStudynoteRecyclerAdapter(ArrayList<Post> posts_param, ImageFetcher imageFetcher, Context context) {
        studynotes = posts_param;
        this.imageFetcher = imageFetcher;
        this.context = context;
    }

    @Override
    public PortletStudynoteRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.portlet_item_studynote, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        if (!studynotes.get(position).getJob_name().equals("")) {
            holder.genre_name.setVisibility(View.GONE);
            holder.job_name.setVisibility(View.VISIBLE);
        } else {
            holder.genre_name.setVisibility(View.VISIBLE);
            holder.job_name.setVisibility(View.GONE);
        }
        holder.genre_name.setText(studynotes.get(position).getGenre_name());
        holder.job_name.setText(studynotes.get(position).getJob_name());
        holder.price.setText(studynotes.get(position).getPrice());
        holder.duration.setText(studynotes.get(position).getDuration());
        holder.title.setText(studynotes.get(position).getTitle());
        holder.creator_name.setText(studynotes.get(position).getCreator_name());
        imageFetcher.setImageToImageView(studynotes.get(position).getWave_image_file_url(), holder.wave_image, 104);
        holder.audio_file_url = studynotes.get(position).getAudio_file_url();
        holder.post_sn = studynotes.get(position).getPost_sn();
        holder.post_snTextView.setText(holder.post_sn);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                ((MainActivity) context).viewStudynote(holder.post_sn);
                ((MainActivity) context).playMediaPlayer(studynotes.get(position));
            }
        });

        // TODO : Study note portlet link UI(or interaction 표준화

    }

    @Override
    public int getItemCount() {
        return studynotes.size();
    }

}
