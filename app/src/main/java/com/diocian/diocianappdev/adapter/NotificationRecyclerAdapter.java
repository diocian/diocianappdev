package com.diocian.diocianappdev.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.diocian.diocianappdev.MainActivity;
import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.model.Notification;
import com.diocian.diocianappdev.util.GlobalConstants;
import com.diocian.diocianappdev.util.HttpRequest;
import com.diocian.diocianappdev.util.ImageFetcher;
import com.diocian.diocianappdev.util.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class NotificationRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Notification> notifications;
    private ImageFetcher imageFetcher;
    private Context context;

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private static final int TYPE_FOOTER = 2;

    private boolean isFooter = true;

    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        public HeaderViewHolder(View view) {
            super(view);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout notificationRelativeLayout;
        public TextView descriptionTextView;
        public TextView createdDatetimeTextView;
        public String notification_sn;
        public String action_cd;
        public String post_dc;
        public String target_sn;

        public ViewHolder(View view) {
            super(view);
            notificationRelativeLayout = (LinearLayout) view.findViewById(R.id.notificationLinearLayout);
            descriptionTextView = (TextView) view.findViewById(R.id.descriptionTextView);
            createdDatetimeTextView = (TextView) view.findViewById(R.id.createdDatetimeTextView);
        }
    }

    public class FooterViewHolder extends RecyclerView.ViewHolder {
        public FooterViewHolder(View view) {
            super(view);
        }
    }

    public NotificationRecyclerAdapter(ArrayList<Notification> posts_param, ImageFetcher imageFetcher, Context context) {
        notifications = posts_param;
        this.imageFetcher = imageFetcher;
        this.context = context;
    }

    @Override
    public int getItemViewType(int position) {
        if (isFooter) {
            if (position == 0) {
                return TYPE_HEADER;
            } else if (position == (notifications.size() + 1)) {
                return TYPE_FOOTER;
            }
        } else {
            if (position == 0) {
                return TYPE_HEADER;
            }
        }
        return TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_header_none, parent, false);
            return new HeaderViewHolder(view);
        } else if (viewType == TYPE_FOOTER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_footer, parent, false);
            return new FooterViewHolder(view);
        }
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_notification, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            final ViewHolder itemHolder = (ViewHolder) holder;
            Notification notification = notifications.get(position - 1);

            itemHolder.descriptionTextView.setText(notification.getDescription());
            itemHolder.createdDatetimeTextView.setText(notification.getCreated_datetime());
            itemHolder.action_cd = notification.getAction_cd();
            itemHolder.post_dc = notification.getPost_dc();
            itemHolder.target_sn = notification.getTarget_sn();
            itemHolder.notification_sn = notification.getNotification_sn();

            if (notification.getRead_yn().equals("Y")) {
                itemHolder.descriptionTextView.setTextColor(ContextCompat.getColor(context, R.color.BlackDisabled));
                itemHolder.createdDatetimeTextView.setTextColor(ContextCompat.getColor(context, R.color.BlackDisabled));
            }

            itemHolder.notificationRelativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HttpRequest httpRequest = new HttpRequest(context);
                    HttpRequest.HttpGetRequestTask musicHttpGetRequestTask = httpRequest.newHttpGetRequestTask(new HttpRequest.CallbackIf() {
                        @Override
                        public void callback(JSONObject jsonObject, Context context) {
                            HttpRequest httpRequest = new HttpRequest(context);
                            HttpRequest.HttpGetRequestTask musicHttpGetRequestTask = httpRequest.newHttpGetRequestTask(new HttpRequest.CallbackIf() {
                                @Override
                                public void callback(JSONObject jsonObject, Context context) {
                                    try {
                                        Utils.setNotificationCount(context, jsonObject.getString("count"));
                                        String action_cd = itemHolder.action_cd;
                                        String post_dc = itemHolder.post_dc;
                                        String target_sn = itemHolder.target_sn;
                                        MainActivity mainActivity = (MainActivity) context;
                                        if (action_cd.equals("follow")) {
                                            mainActivity.viewMember(target_sn);
                                        } else {
                                            if (post_dc.equals("music")) {
                                                mainActivity.viewMusic(target_sn);
                                            } else if (post_dc.equals("video")) {
                                                mainActivity.viewVideo(target_sn);
                                            } else if (post_dc.equals("study note")) {
                                                mainActivity.viewStudynote(target_sn);
                                            } else if (post_dc.equals("studio")) {
                                                mainActivity.viewStudio(target_sn);
                                            } else if (post_dc.equals("market")) {
                                                mainActivity.viewMarket(target_sn);
                                            } else if (post_dc.equals("article")) {
                                                mainActivity.viewArticle(target_sn);
                                            }
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                            musicHttpGetRequestTask.execute(GlobalConstants.DOMAIN + "/getNotifiactionCountByMember.do?target_member_sn=" + ((MainActivity) context).getPreferences("member_sn"));
                        }
                    });
                    musicHttpGetRequestTask.execute(GlobalConstants.DOMAIN + "/setNotificationRead.do?read_yn=Y&notification_sn=" + itemHolder.notification_sn);

                }
            });

        } else if (holder instanceof HeaderViewHolder) {
            StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
            layoutParams.setFullSpan(true);
        } else if (holder instanceof FooterViewHolder) {
            StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
            layoutParams.setFullSpan(true);
        }
    }

    @Override
    public int getItemCount() {
        if (isFooter) {
            return notifications.size() + 2;
        }
        return notifications.size() + 1;
    }

    public void removeFooter() {
        isFooter = false;
    }

}
