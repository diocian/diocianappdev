package com.diocian.diocianappdev.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.diocian.diocianappdev.MainActivity;
import com.diocian.diocianappdev.model.Post;
import com.diocian.diocianappdev.R;
import com.diocian.diocianappdev.util.ImageFetcher;

import java.util.ArrayList;

public class PortletMarketRecyclerAdapter extends RecyclerView.Adapter<PortletMarketRecyclerAdapter.ViewHolder> {

    private ArrayList<Post> markets;
    private ImageFetcher imageFetcher;
    private Context context;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView price;
        public ImageView image;
        public TextView title;
        public TextView creator_name;
        public String post_sn;

        public ViewHolder(View view) {
            super(view);
            price = (TextView) view.findViewById(R.id.price);
            image = (ImageView) view.findViewById(R.id.image);
            title = (TextView) view.findViewById(R.id.title);
            creator_name = (TextView) view.findViewById(R.id.creator_name);
        }
    }

    public PortletMarketRecyclerAdapter(ArrayList<Post> posts_param, ImageFetcher imageFetcher, Context context) {
        markets = posts_param;
        this.imageFetcher = imageFetcher;
        this.context = context;
    }

    @Override
    public PortletMarketRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.portlet_item_market, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.price.setText(markets.get(position).getPrice());
        holder.title.setText(markets.get(position).getTitle());
        holder.creator_name.setText(markets.get(position).getCreator_name());
        imageFetcher.setImageToImageView(markets.get(position).getImage_file_url(), holder.image, 104);

        holder.post_sn = markets.get(position).getPost_sn();
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) context).viewMarket(holder.post_sn);
            }
        });
    }

    @Override
    public int getItemCount() {
        return markets.size();
    }

}
